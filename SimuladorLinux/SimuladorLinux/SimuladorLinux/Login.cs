﻿using System;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Linq;
using MySql.Data.MySqlClient;

namespace SimuladorLinux
{
	public partial class frmLogin : Form
	{
		static public frmLogin login;

		public frmLogin()
		{
			InitializeComponent();
			login = this;
			frmContenedor.ConfigurarMdiParent(this);
		}

		private void Login_Load(object sender, EventArgs e)
		{
			if (this.MdiParent != null)
			{
				MdiClient mdiClient = this.MdiParent.Controls.OfType<MdiClient>().FirstOrDefault();
				Size tamañoMDI = new Size(mdiClient.Size.Width /*- 5*/, mdiClient.Size.Height /*- 5*/);
				this.Size = tamañoMDI;
			}
			this.txtUsuario.Focus();
		}

		private void btnIniciarSesion_Click(object sender, EventArgs e)
		{
			if (String.IsNullOrEmpty(this.txtUsuario.Text))
			{
				MessageBox.Show("Favor, especifique su usuario y contraseña para ingresar,", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				return;
			}
			string connectionString = ConfigurationManager.ConnectionStrings["mysqlConnection"].ConnectionString;
			MySqlConnection con = new MySqlConnection(connectionString);
			try
			{
				con.Open();
				MySqlCommand cmd = new MySqlCommand(
					"SELECT Id, Nombre, Usuario, Contrasenia, Tipo FROM Usuarios " +
					"WHERE Usuario = @Usuario AND Contrasenia = @Contrasenia",
					con
				);
				cmd.Parameters.AddWithValue("Usuario", this.txtUsuario.Text);
				cmd.Parameters.AddWithValue("Contrasenia", this.txtContrasenia.Text);
				MySqlDataAdapter sda = new MySqlDataAdapter(cmd);
				DataTable dt = new DataTable();
				sda.Fill(dt);

				if (dt.Rows.Count == 1)
				{
					this.Hide();
					string id = dt.Rows[0][0].ToString();
					string nombre = dt.Rows[0][1].ToString();
					string usuario = dt.Rows[0][2].ToString();
					string contrasenia = dt.Rows[0][3].ToString();
					string tipo = dt.Rows[0][4].ToString();
					Usuario.actual = new Usuario(id, nombre, usuario, contrasenia, tipo);
					
					cmd = new MySqlCommand();
					cmd.Connection = con;
					cmd.Parameters.AddWithValue("@Id_Usuario", id);
					cmd.Parameters.AddWithValue("@Usuario", usuario);
					cmd.Parameters.AddWithValue("@Nombre", nombre);
					cmd.CommandText = "Insert HistorialAccesos(Fecha, Id_Usuario, Usuario, Nombre) Values (Now(), @Id_Usuario, @Usuario, @Nombre)";
					cmd.ExecuteNonQuery();
					this.Hide();

					frmExplorador.VerificarCrearCarpeta(frmExplorador.RutaInicio());
					frmEscritorio escritorio = new frmEscritorio();
					escritorio.Show();
					this.Close();
				}
				else
				{
					MessageBox.Show("Revise los datos ingresados e intente de nuevo.", "Usuario y/o Contraseña Incorrecta", MessageBoxButtons.OK, MessageBoxIcon.Error);
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show("Ha ocurrido un error accediendo: \n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
			finally
			{
				if (con.State != ConnectionState.Closed)
				{
					con.Close();
				}
			}
		}

		private void frmLogin_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (e.CloseReason == CloseReason.UserClosing)
				e.Cancel = true;
		}

		private void btnAyuda_Click(object sender, EventArgs e)
		{
			frmAyuda.AbrirAyuda("login");
		}
	}
}

