﻿namespace SimuladorLinux
{
	partial class frmCalculadora
	{
		/// <summary>
		/// Variable del diseñador requerida.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Limpiar los recursos que se estén utilizando.
		/// </summary>
		/// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Código generado por el Diseñador de Windows Forms

		/// <summary>
		/// Método necesario para admitir el Diseñador. No se puede modificar
		/// el contenido del método con el editor de código.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCalculadora));
			this.btnresultado = new System.Windows.Forms.Button();
			this.btnraiz = new System.Windows.Forms.Button();
			this.btn6 = new System.Windows.Forms.Button();
			this.btnpunto = new System.Windows.Forms.Button();
			this.btn3 = new System.Windows.Forms.Button();
			this.btn9 = new System.Windows.Forms.Button();
			this.btnsuma = new System.Windows.Forms.Button();
			this.btn2 = new System.Windows.Forms.Button();
			this.btn5 = new System.Windows.Forms.Button();
			this.btnresta = new System.Windows.Forms.Button();
			this.btndividir = new System.Windows.Forms.Button();
			this.btnmultiplicar = new System.Windows.Forms.Button();
			this.btn8 = new System.Windows.Forms.Button();
			this.btn0 = new System.Windows.Forms.Button();
			this.btn1 = new System.Windows.Forms.Button();
			this.btn4 = new System.Windows.Forms.Button();
			this.btn7 = new System.Windows.Forms.Button();
			this.btn_limpiar = new System.Windows.Forms.Button();
			this.pantalla = new System.Windows.Forms.TextBox();
			this.btnAyuda = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// btnresultado
			// 
			this.btnresultado.Location = new System.Drawing.Point(180, 271);
			this.btnresultado.Name = "btnresultado";
			this.btnresultado.Size = new System.Drawing.Size(50, 50);
			this.btnresultado.TabIndex = 38;
			this.btnresultado.Text = "=";
			this.btnresultado.UseVisualStyleBackColor = true;
			this.btnresultado.Click += new System.EventHandler(this.btnresultado_Click);
			// 
			// btnraiz
			// 
			this.btnraiz.Location = new System.Drawing.Point(124, 47);
			this.btnraiz.Name = "btnraiz";
			this.btnraiz.Size = new System.Drawing.Size(50, 50);
			this.btnraiz.TabIndex = 37;
			this.btnraiz.Text = "Raiz";
			this.btnraiz.UseVisualStyleBackColor = true;
			this.btnraiz.Click += new System.EventHandler(this.btnraiz_Click);
			// 
			// btn6
			// 
			this.btn6.Location = new System.Drawing.Point(124, 159);
			this.btn6.Name = "btn6";
			this.btn6.Size = new System.Drawing.Size(50, 50);
			this.btn6.TabIndex = 36;
			this.btn6.Text = "6";
			this.btn6.UseVisualStyleBackColor = true;
			this.btn6.Click += new System.EventHandler(this.btn6_Click);
			// 
			// btnpunto
			// 
			this.btnpunto.Location = new System.Drawing.Point(124, 271);
			this.btnpunto.Name = "btnpunto";
			this.btnpunto.Size = new System.Drawing.Size(50, 50);
			this.btnpunto.TabIndex = 35;
			this.btnpunto.Text = ".";
			this.btnpunto.UseVisualStyleBackColor = true;
			this.btnpunto.Click += new System.EventHandler(this.btnpunto_Click);
			// 
			// btn3
			// 
			this.btn3.Location = new System.Drawing.Point(124, 215);
			this.btn3.Name = "btn3";
			this.btn3.Size = new System.Drawing.Size(50, 50);
			this.btn3.TabIndex = 34;
			this.btn3.Text = "3";
			this.btn3.UseVisualStyleBackColor = true;
			this.btn3.Click += new System.EventHandler(this.btn3_Click);
			// 
			// btn9
			// 
			this.btn9.Location = new System.Drawing.Point(124, 103);
			this.btn9.Name = "btn9";
			this.btn9.Size = new System.Drawing.Size(50, 50);
			this.btn9.TabIndex = 33;
			this.btn9.Text = "9";
			this.btn9.UseVisualStyleBackColor = true;
			this.btn9.Click += new System.EventHandler(this.btn9_Click);
			// 
			// btnsuma
			// 
			this.btnsuma.Location = new System.Drawing.Point(180, 215);
			this.btnsuma.Name = "btnsuma";
			this.btnsuma.Size = new System.Drawing.Size(50, 50);
			this.btnsuma.TabIndex = 32;
			this.btnsuma.Text = "+";
			this.btnsuma.UseVisualStyleBackColor = true;
			this.btnsuma.Click += new System.EventHandler(this.btnsuma_Click);
			// 
			// btn2
			// 
			this.btn2.Location = new System.Drawing.Point(68, 215);
			this.btn2.Name = "btn2";
			this.btn2.Size = new System.Drawing.Size(50, 50);
			this.btn2.TabIndex = 31;
			this.btn2.Text = "2";
			this.btn2.UseVisualStyleBackColor = true;
			this.btn2.Click += new System.EventHandler(this.btn2_Click);
			// 
			// btn5
			// 
			this.btn5.Location = new System.Drawing.Point(68, 159);
			this.btn5.Name = "btn5";
			this.btn5.Size = new System.Drawing.Size(50, 50);
			this.btn5.TabIndex = 30;
			this.btn5.Text = "5";
			this.btn5.UseVisualStyleBackColor = true;
			this.btn5.Click += new System.EventHandler(this.btn5_Click);
			// 
			// btnresta
			// 
			this.btnresta.Location = new System.Drawing.Point(180, 159);
			this.btnresta.Name = "btnresta";
			this.btnresta.Size = new System.Drawing.Size(50, 50);
			this.btnresta.TabIndex = 29;
			this.btnresta.Text = "-";
			this.btnresta.UseVisualStyleBackColor = true;
			this.btnresta.Click += new System.EventHandler(this.btnresta_Click);
			// 
			// btndividir
			// 
			this.btndividir.Location = new System.Drawing.Point(180, 47);
			this.btndividir.Name = "btndividir";
			this.btndividir.Size = new System.Drawing.Size(50, 50);
			this.btndividir.TabIndex = 28;
			this.btndividir.Text = "/";
			this.btndividir.UseVisualStyleBackColor = true;
			this.btndividir.Click += new System.EventHandler(this.btndividir_Click);
			// 
			// btnmultiplicar
			// 
			this.btnmultiplicar.Location = new System.Drawing.Point(180, 103);
			this.btnmultiplicar.Name = "btnmultiplicar";
			this.btnmultiplicar.Size = new System.Drawing.Size(50, 50);
			this.btnmultiplicar.TabIndex = 27;
			this.btnmultiplicar.Text = "*";
			this.btnmultiplicar.UseVisualStyleBackColor = true;
			this.btnmultiplicar.Click += new System.EventHandler(this.btnmultiplicar_Click);
			// 
			// btn8
			// 
			this.btn8.Location = new System.Drawing.Point(68, 103);
			this.btn8.Name = "btn8";
			this.btn8.Size = new System.Drawing.Size(50, 50);
			this.btn8.TabIndex = 26;
			this.btn8.Text = "8";
			this.btn8.UseVisualStyleBackColor = true;
			this.btn8.Click += new System.EventHandler(this.btn8_Click);
			// 
			// btn0
			// 
			this.btn0.Location = new System.Drawing.Point(12, 271);
			this.btn0.Name = "btn0";
			this.btn0.Size = new System.Drawing.Size(106, 50);
			this.btn0.TabIndex = 25;
			this.btn0.Text = "0";
			this.btn0.UseVisualStyleBackColor = true;
			this.btn0.Click += new System.EventHandler(this.btn0_Click);
			// 
			// btn1
			// 
			this.btn1.Location = new System.Drawing.Point(12, 215);
			this.btn1.Name = "btn1";
			this.btn1.Size = new System.Drawing.Size(50, 50);
			this.btn1.TabIndex = 24;
			this.btn1.Text = "1";
			this.btn1.UseVisualStyleBackColor = true;
			this.btn1.Click += new System.EventHandler(this.btn1_Click);
			// 
			// btn4
			// 
			this.btn4.Location = new System.Drawing.Point(12, 159);
			this.btn4.Name = "btn4";
			this.btn4.Size = new System.Drawing.Size(50, 50);
			this.btn4.TabIndex = 23;
			this.btn4.Text = "4";
			this.btn4.UseVisualStyleBackColor = true;
			this.btn4.Click += new System.EventHandler(this.btn4_Click);
			// 
			// btn7
			// 
			this.btn7.Location = new System.Drawing.Point(12, 103);
			this.btn7.Name = "btn7";
			this.btn7.Size = new System.Drawing.Size(50, 50);
			this.btn7.TabIndex = 22;
			this.btn7.Text = "7";
			this.btn7.UseVisualStyleBackColor = true;
			this.btn7.Click += new System.EventHandler(this.btn7_Click);
			// 
			// btn_limpiar
			// 
			this.btn_limpiar.Location = new System.Drawing.Point(68, 47);
			this.btn_limpiar.Name = "btn_limpiar";
			this.btn_limpiar.Size = new System.Drawing.Size(50, 50);
			this.btn_limpiar.TabIndex = 21;
			this.btn_limpiar.Text = "CE";
			this.btn_limpiar.UseVisualStyleBackColor = true;
			this.btn_limpiar.Click += new System.EventHandler(this.btn_limpiar_Click);
			// 
			// pantalla
			// 
			this.pantalla.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.pantalla.Location = new System.Drawing.Point(12, 12);
			this.pantalla.Name = "pantalla";
			this.pantalla.ReadOnly = true;
			this.pantalla.Size = new System.Drawing.Size(218, 29);
			this.pantalla.TabIndex = 20;
			this.pantalla.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// btnAyuda
			// 
			this.btnAyuda.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnAyuda.BackColor = System.Drawing.Color.Transparent;
			this.btnAyuda.BackgroundImage = global::SimuladorLinux.Properties.Resources.help_person;
			this.btnAyuda.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.btnAyuda.FlatAppearance.BorderSize = 0;
			this.btnAyuda.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnAyuda.Location = new System.Drawing.Point(12, 47);
			this.btnAyuda.Name = "btnAyuda";
			this.btnAyuda.Size = new System.Drawing.Size(50, 50);
			this.btnAyuda.TabIndex = 40;
			this.btnAyuda.UseVisualStyleBackColor = false;
			this.btnAyuda.Click += new System.EventHandler(this.btnAyuda_Click);
			// 
			// frmCalculadora
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.ClientSize = new System.Drawing.Size(244, 329);
			this.Controls.Add(this.btnAyuda);
			this.Controls.Add(this.btnresultado);
			this.Controls.Add(this.btnraiz);
			this.Controls.Add(this.btn6);
			this.Controls.Add(this.btnpunto);
			this.Controls.Add(this.btn3);
			this.Controls.Add(this.btn9);
			this.Controls.Add(this.btnsuma);
			this.Controls.Add(this.btn2);
			this.Controls.Add(this.btn5);
			this.Controls.Add(this.btnresta);
			this.Controls.Add(this.btndividir);
			this.Controls.Add(this.btnmultiplicar);
			this.Controls.Add(this.btn8);
			this.Controls.Add(this.btn0);
			this.Controls.Add(this.btn1);
			this.Controls.Add(this.btn4);
			this.Controls.Add(this.btn7);
			this.Controls.Add(this.btn_limpiar);
			this.Controls.Add(this.pantalla);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.Name = "frmCalculadora";
			this.ShowInTaskbar = false;
			this.Text = "Calculadora";
			this.Load += new System.EventHandler(this.frmCalculadora_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button btnresultado;
		private System.Windows.Forms.Button btnraiz;
		private System.Windows.Forms.Button btn6;
		private System.Windows.Forms.Button btnpunto;
		private System.Windows.Forms.Button btn3;
		private System.Windows.Forms.Button btn9;
		private System.Windows.Forms.Button btnsuma;
		private System.Windows.Forms.Button btn2;
		private System.Windows.Forms.Button btn5;
		private System.Windows.Forms.Button btnresta;
		private System.Windows.Forms.Button btndividir;
		private System.Windows.Forms.Button btnmultiplicar;
		private System.Windows.Forms.Button btn8;
		private System.Windows.Forms.Button btn0;
		private System.Windows.Forms.Button btn1;
		private System.Windows.Forms.Button btn4;
		private System.Windows.Forms.Button btn7;
		private System.Windows.Forms.Button btn_limpiar;
		private System.Windows.Forms.TextBox pantalla;
		private System.Windows.Forms.Button btnAyuda;
	}
}

