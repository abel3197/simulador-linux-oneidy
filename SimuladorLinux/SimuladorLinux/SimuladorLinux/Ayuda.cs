﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SimuladorLinux
{
	public partial class frmAyuda : Form
	{
		static string rutaAyuda = frmExplorador.RutaBase() + "Sistema/Ayuda/";

		public frmAyuda()
		{
			InitializeComponent();
			frmContenedor.ConfigurarMdiParent(this);
		}

		private void Ayuda_Load(object sender, EventArgs e)
		{
			this.tvwContenidos.Nodes[0].ExpandAll();
		}

		private void tvwContenidos_AfterSelect(object sender, TreeViewEventArgs e)
		{
			ActualizarSeleccionado(e.Node.Name);
		}

		static public void AbrirAyuda(string pantalla)
		{
			frmAyuda ayuda = new frmAyuda();
			foreach (TreeNode nodo in ayuda.tvwContenidos.Nodes[0].Nodes)
			{
				if (nodo.Name == pantalla)
				{
					ayuda.tvwContenidos.SelectedNode = nodo;
					break;
				}
			}
			ayuda.Show();
			ayuda.BringToFront();
		}

		void ActualizarSeleccionado(string pantalla)
		{
			Image imagenAyuda = (Image)Properties.Resources.ResourceManager.GetObject(pantalla + "Ayuda");
			this.imgContenido.BackgroundImage = imagenAyuda;
		}
	}
}
