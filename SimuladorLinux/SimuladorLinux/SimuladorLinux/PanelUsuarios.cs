﻿using MySql.Data.MySqlClient;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace SimuladorLinux
{
	public partial class frmPanelUsuarios : Form
	{
		public frmPanelUsuarios()
		{
			InitializeComponent();
			frmContenedor.ConfigurarMdiParent(this);
		}

		private void frmRegistroUsuarios_Load(object sender, EventArgs e)
		{
			CargarUsuarios();
			if (Usuario.actual.nombre == "Invitado")
			{
				this.btnCambiarFondo.Visible = false;
			}
			if (Usuario.actual.tipo != "Administrador")
			{
				this.btnNuevo.Visible = false;
			}
		}

		private void btnNuevo_Click(object sender, EventArgs e)
		{
			frmDetalleUsuario detalle = new frmDetalleUsuario();
			detalle.CargarUsuarios = CargarUsuarios;
			detalle.ShowDialog();
		}

		private void lstUsuarios_MouseDoubleClick(object sender, MouseEventArgs e)
		{
			bool usuarioEsAdministrador = Usuario.actual.tipo == "Administrador";
			bool hayUsuarioSeleccionado = lstUsuarios.SelectedItems.Count == 1;
			if (usuarioEsAdministrador && hayUsuarioSeleccionado)
			{
				ListViewItem.ListViewSubItemCollection props = lstUsuarios.SelectedItems[0].SubItems;
				frmDetalleUsuario detalle = new frmDetalleUsuario(
					props[0].Text, props[1].Text, props[2].Text, (string)props[3].Tag, props[4].Text
				);
				detalle.CargarUsuarios = CargarUsuarios;
				detalle.ShowDialog();
			}
		}

		void CargarUsuarios()
		{
			string connectionString = ConfigurationManager.ConnectionStrings["mysqlConnection"].ConnectionString;
			MySqlConnection con = new MySqlConnection(connectionString);
			try
			{
				con.Open();
				MySqlCommand cmd = new MySqlCommand(
					"SELECT Id, Nombre, Usuario, Contrasenia, Tipo FROM Usuarios",
					con
				);
				MySqlDataAdapter sda = new MySqlDataAdapter(cmd);
				DataTable dt = new DataTable();
				sda.Fill(dt);

				lstUsuarios.Items.Clear();
				for (int i = 0; i < dt.Rows.Count; i++)
				{
					string id = dt.Rows[i][0].ToString();
					string nombre = dt.Rows[i][1].ToString();
					string usuario = dt.Rows[i][2].ToString();
					string contrasenia = dt.Rows[i][3].ToString();
					string tipo = dt.Rows[i][4].ToString();
					ListViewItem fila = new ListViewItem(id);
					fila.SubItems.Add(usuario);
					fila.SubItems.Add(nombre);
					fila.SubItems.Add("**********").Tag = contrasenia;
					fila.SubItems.Add(tipo);
					lstUsuarios.Items.Add(fila);
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show("No se pudo consultar registros: \n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
			finally
			{
				if (con.State != ConnectionState.Closed)
				{
					con.Close();
				}
			}
		}

		private void lstUsuarios_SelectedIndexChanged(object sender, EventArgs e)
		{

		}

		private void btnCambiarFondo_Click(object sender, EventArgs e)
		{
			OpenFileDialog fileDialog = new OpenFileDialog();
			fileDialog.Title = "Seleccionar foto para el fondo de escritorio";
			fileDialog.Multiselect = false;
			fileDialog.InitialDirectory = frmExplorador.RutaUsuario();
			string imagenes = "*.png;*.bpm;*.jpg;*.jpeg;*.ico;*.gif;*.tif;*.tiff";
			fileDialog.Filter = "Imágenes|" + imagenes + "|Todos los archivos|*.*";
			fileDialog.CheckFileExists = true;
			DialogResult opcion = fileDialog.ShowDialog(this);
			if (opcion == DialogResult.OK)
			{
				string nombreArchivo = fileDialog.SafeFileName;
				string rutaArchivo = fileDialog.FileName;
				try
				{
					frmEscritorio escritorio = frmEscritorio.escritorio;
					if (escritorio != null)
					{
						Image imagen = Image.FromFile(rutaArchivo);
						imagen.Dispose();
						string rutaFondo = frmExplorador.RutaUsuario() + "fondo";
						escritorio.BackgroundImage.Dispose();
						File.Copy(rutaArchivo, rutaFondo, true);
						Image imagenNueva = Image.FromFile(rutaFondo);
						escritorio.CambiarImagenFondo(imagenNueva);
					}
				}
				catch (Exception ex)
				{
					MessageBox.Show(
						"No se pudo cambiar el fondo de pantalla con el archivo "
						+ nombreArchivo + "\n" + ex.Message,
						"Error procesando archivo como imagen",
						MessageBoxButtons.OK,
						MessageBoxIcon.Error
					);
				}
			}
		}

		private void btnAyuda_Click(object sender, EventArgs e)
		{
			frmAyuda.AbrirAyuda("usuarios");
		}
	}
}
