using System;
using System.Collections.Generic;
using System.Windows.Forms;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using Sel;

namespace BuscarRedesSociales
{
	public partial class SocialMediaMainForm : Form
	{
		private bool IsFilePathOk { get; set; }
		private bool IsNewFileFolderOk { get; set; }
		private string[] ValidHeaders { get; set; }
		private string[] NewFileHeaders { get; set; }

		public SocialMediaMainForm()
		{
			InitializeComponent();
			IsFilePathOk = false;
			ValidHeaders = new string[] { "NOMBRE_APELLIDO" };
			NewFileHeaders = new string[] { "NOMBRE_APELLIDO", "URL FACEBOOK", "URL LINKEDIN", "URL TWITTER", "URL INSTAGRAM" };
		}

		private void BtnInitProcess_Click(object sender, EventArgs e)
		{
			if (!Validations())
				return;

			Excel currentExcelFile = null;
			Excel newFile = null;
			ChromeDriver driver = null;

			try
			{
				currentExcelFile = Excel.Load(path: TBXloadFile.Text);
				newFile = new Excel(sheetName: "Resultados");
				newFile.SetHeaders(NewFileHeaders);
				var r = currentExcelFile.GetHeaders();

				if (!currentExcelFile.HasHeader(ValidHeaders))
				{
					PrintMessage("El archivo no es valido", "Alerta", MessageBoxIcon.Warning);
					return;
				}

				driver = new ChromeDriver();
				driver.Manage().Window.Maximize();

				foreach (Dictionary<string, object> row in currentExcelFile.Extract())
				{

					string name = row["NOMBRE_APELLIDO"].ToString();
					string urlFacebook = GetFacebookUrl(driver, name);
					string urlLinkedin = GetLinkedinUrl(driver, name);
					string urlTwitter = GetTwitterUrl(driver, name);
					string urlInstagram = GetInstagramUrl(driver, name);

					List<string> data = new List<string>() { name, urlFacebook, urlLinkedin, urlTwitter, urlInstagram };

					//adding the new data as a row
					newFile.Append(data);
				}

				newFile.Save(path: TBXnewFilePath.Text);
				driver.Dispose();

				PrintMessage("Se ha completado el proceso correctamente", "Mensaje", MessageBoxIcon.Information);
			}
			catch (Exception ex)
			{
				driver.Dispose();
				PrintMessage(string.Format("Ha ocurrido un error:\n{0}", ex.Message), "Error", MessageBoxIcon.Error);
			}

		}

		private string GetFacebookUrl(IWebDriver driver, string name)
		{
			driver.Navigate().GoToUrl("https://facebook.com/public/" + name);
			System.Threading.Thread.Sleep(3000);
			try
			{
				string firstPersonXpathExpression = XPathExpression
					.From("a")
					.WhereAttribute("role", "presentation")
					.GetExpression();
				IWebElement firstPersonElement = driver.FindElement(By.XPath(firstPersonXpathExpression));
				String url = firstPersonElement.GetAttribute("href");
				return url;
			}
			catch (Exception ex)
			{
				return "Ha ocurrido un error al buscar el search input de Facebook";
			}
		}

		private string GetLinkedinUrl(IWebDriver driver, string name)
		{
			return "LINKEDIN";
		}

		private string GetTwitterUrl(IWebDriver driver, string name)
		{
			driver.Navigate().GoToUrl("https://twitter.com/explore");

			System.Threading.Thread.Sleep(5000);

			string searchInputXpathExpression = XPathExpression
				.From("input")
				.WhereAttributeContains("placeholder", "Buscar en Twitter").Or()
				.WhereAttributeContains("placeholder", "Buscar").Or()
				.WhereAttributeContains("placeholder", "Search")
				.GetExpression();


			try
			{
				IWebElement searchInputElement = driver.FindElement(By.XPath(searchInputXpathExpression));
				searchInputElement.Clear();

				searchInputElement.SendKeys(name);
				System.Threading.Thread.Sleep(3000);


				//Buscando si hay un usuario
				try
				{

					string userUrlXpathExpression = XPathExpression.From("span")
						.WhereDescendant(XPathExpression.FromAny()
						.WhereTextContains(name).Or()
						.WhereTextContains(name.ToLower())
						)
						.GetExpression();

					IWebElement userDivElement = driver.FindElement(By.XPath(userUrlXpathExpression));
					userDivElement.Click();

					return driver.Url;
				}
				catch (Exception)
				{
					return "No existe un usuario con ese nombre";
				}

			}
			catch (Exception)
			{
				throw new Exception("Ha ocurrido un error al buscar el search input de twitter");
			}
		}

		private string GetInstagramUrl(IWebDriver driver, string name)
		{
			driver.Navigate().GoToUrl("https://www.instagram.com/explore/tags/buscar/");
			System.Threading.Thread.Sleep(5000);

			string searchInputXpathExpression = XPathExpression
				.From("input")
				.WhereAttributeContains("placeholder", "Buscar").Or()
				.WhereAttributeContains("placeholder", "Busca").Or()
				.WhereAttributeContains("placeholder", "Search")
				.GetExpression();


			try
			{
				IWebElement searchInputElement = driver.FindElement(By.XPath(searchInputXpathExpression));
				searchInputElement.Clear();

				searchInputElement.SendKeys(name);
				System.Threading.Thread.Sleep(3000);


				//Buscando si hay un usuario
				try
				{

					string userUrlXpathExpression = XPathExpression
						.From("a").WhereDescendant(XPathExpression
						.FromAny()
							.WhereTextContains(name.Replace(" ", "_")).Or()
							.WhereTextContains(name.Replace(" ", "")).Or()
							.WhereTextContains(name.Replace(" ", ".")).Or()
							.WhereTextContains(name.Replace(" ", "_").ToLower()).Or()
							.WhereTextContains(name.Replace(" ", "").ToLower()).Or()
							.WhereTextContains(name.Replace(" ", ".").ToLower()
							)
						).GetExpression();

					IWebElement userAelement = driver.FindElement(By.XPath(userUrlXpathExpression));
					return userAelement.GetAttribute("href");

				}
				catch (Exception ex)
				{
					//throw new Exception("No existe un usuario con ese nombre", ex);
					return "No existe un usuario con ese nombre";
				}

			}
			catch (Exception ex)
			{
				//throw new Exception("Ha ocurrido un error al buscar el search input de instagram", ex);
				return "Ha ocurrido un error al buscar el search input de instagram";
			}
		}



		private void BTNLoadFile_Click(object sender, EventArgs e)
		{
			try
			{
				using (OpenFileDialog openFileDialog = new OpenFileDialog())
				{
					//Settings the file to look for
					openFileDialog.InitialDirectory = "c:\\";
					openFileDialog.Filter = "Excel Worksheets 2007 (*.xlsx)|*.xlsx";
					openFileDialog.FilterIndex = 1;
					openFileDialog.RestoreDirectory = true;

					DialogResult result = openFileDialog.ShowDialog();

					//If the dialogResult is ok and a file has been set then go ahead
					if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(openFileDialog.FileName))
					{
						TBXloadFile.Text = openFileDialog.FileName;
						IsFilePathOk = true;
					}
					//If filePath texbox is not null then go ahead
					else if (!string.IsNullOrWhiteSpace(TBXloadFile.Text))
						IsFilePathOk = true;
					else
						IsFilePathOk = false;
				}
			}
			catch (Exception ex)
			{
				PrintMessage(string.Format("Ha ocurrido un error:\n{0}", ex), "Error", MessageBoxIcon.Error);
			}

		}

		private void BTNnewFilePath_Click(object sender, EventArgs e)
		{
			try
			{
				SaveFileDialog saveFileDialog = new SaveFileDialog();
				saveFileDialog.InitialDirectory = "c:\\";
				saveFileDialog.Filter = "Excel Worksheets 2007 (*.xlsx)|*.xlsx";
				saveFileDialog.FilterIndex = 1;
				saveFileDialog.RestoreDirectory = true;

				DialogResult result = saveFileDialog.ShowDialog();

				//If the dialogResult is ok and a file has been set then go ahead
				if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(saveFileDialog.FileName))
				{
					TBXnewFilePath.Text = saveFileDialog.FileName;
					IsNewFileFolderOk = true;
				}
				//If filePath texbox is not null then go ahead
				else if (!string.IsNullOrWhiteSpace(TBXnewFilePath.Text))
					IsNewFileFolderOk = true;
				else
					IsNewFileFolderOk = false;
			}
			catch (Exception ex)
			{
				PrintMessage(string.Format("Ha ocurrido un error:\n{0}", ex), "Error", MessageBoxIcon.Error);
			}

		}

		private bool Validations()
		{

			//Validates if filePath is ok
			if (!IsFilePathOk)
			{
				PrintMessage("Debe elegir el archivo de excel", "Alerta", MessageBoxIcon.Warning);
				return false;
			}

			//Validates if new folder Path is ok
			if (!IsNewFileFolderOk)
			{
				PrintMessage("Debe elegir la ruta para el nuevo archivo", "Alerta", MessageBoxIcon.Warning);
				return false;
			}

			return true;
		}

		private void PrintMessage(string message, string messageType, MessageBoxIcon icon)
		{
			MessageBox.Show(message, messageType, MessageBoxButtons.OK, icon);
		}

		private void BtnCreateFile_Click(object sender, EventArgs e)
		{
			try
			{
				SaveFileDialog saveFileDialog = new SaveFileDialog();
				saveFileDialog.InitialDirectory = "c:\\";
				saveFileDialog.Filter = "Excel Worksheets 2007 (*.xlsx)|*.xlsx";
				saveFileDialog.FilterIndex = 1;
				saveFileDialog.RestoreDirectory = true;

				DialogResult result = saveFileDialog.ShowDialog();

				if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(saveFileDialog.FileName))
				{
					Excel newFile = new Excel();
					newFile.SetHeaders(ValidHeaders);
					newFile.Save(path: saveFileDialog.FileName);

					PrintMessage("Se ha generado la plantilla correctamente", "Mensaje", MessageBoxIcon.Information);
				}
				else
					PrintMessage("Debe elegir una ruta de guardado para la plantilla ", "Alerta", MessageBoxIcon.Warning);

			}
			catch (Exception ex)
			{
				PrintMessage(string.Format("Ha ocurrido un error:\n{0}", ex), "Error", MessageBoxIcon.Error);
			}

		}

	}
}
