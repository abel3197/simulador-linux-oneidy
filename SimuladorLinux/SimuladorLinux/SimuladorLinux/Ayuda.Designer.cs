﻿namespace SimuladorLinux
{
	partial class frmAyuda
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("Login");
			System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("Calculadora");
			System.Windows.Forms.TreeNode treeNode3 = new System.Windows.Forms.TreeNode("Bloc de notas");
			System.Windows.Forms.TreeNode treeNode4 = new System.Windows.Forms.TreeNode("Explorador");
			System.Windows.Forms.TreeNode treeNode5 = new System.Windows.Forms.TreeNode("Panel de usuarios");
			System.Windows.Forms.TreeNode treeNode6 = new System.Windows.Forms.TreeNode("Reproductor");
			System.Windows.Forms.TreeNode treeNode7 = new System.Windows.Forms.TreeNode("Simulador Linux", new System.Windows.Forms.TreeNode[] {
            treeNode1,
            treeNode2,
            treeNode3,
            treeNode4,
            treeNode5,
            treeNode6});
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAyuda));
			this.tvwContenidos = new System.Windows.Forms.TreeView();
			this.lblTitulo = new System.Windows.Forms.Label();
			this.imgContenido = new System.Windows.Forms.PictureBox();
			((System.ComponentModel.ISupportInitialize)(this.imgContenido)).BeginInit();
			this.SuspendLayout();
			// 
			// tvwContenidos
			// 
			this.tvwContenidos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
			this.tvwContenidos.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.tvwContenidos.FullRowSelect = true;
			this.tvwContenidos.HideSelection = false;
			this.tvwContenidos.Location = new System.Drawing.Point(0, 37);
			this.tvwContenidos.Name = "tvwContenidos";
			treeNode1.Name = "login";
			treeNode1.Text = "Login";
			treeNode2.Name = "calculadora";
			treeNode2.Text = "Calculadora";
			treeNode3.Name = "blocnotas";
			treeNode3.Text = "Bloc de notas";
			treeNode4.Name = "explorador";
			treeNode4.Text = "Explorador";
			treeNode5.Name = "usuarios";
			treeNode5.Text = "Panel de usuarios";
			treeNode6.Name = "reproductor";
			treeNode6.Text = "Reproductor";
			treeNode7.Name = "general";
			treeNode7.Text = "Simulador Linux";
			this.tvwContenidos.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode7});
			this.tvwContenidos.Size = new System.Drawing.Size(206, 422);
			this.tvwContenidos.TabIndex = 1;
			this.tvwContenidos.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tvwContenidos_AfterSelect);
			// 
			// lblTitulo
			// 
			this.lblTitulo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblTitulo.Location = new System.Drawing.Point(0, 0);
			this.lblTitulo.Name = "lblTitulo";
			this.lblTitulo.Size = new System.Drawing.Size(206, 34);
			this.lblTitulo.TabIndex = 2;
			this.lblTitulo.Text = "CONTENIDOS";
			this.lblTitulo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// imgContenido
			// 
			this.imgContenido.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.imgContenido.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.imgContenido.Location = new System.Drawing.Point(212, 12);
			this.imgContenido.Name = "imgContenido";
			this.imgContenido.Size = new System.Drawing.Size(518, 433);
			this.imgContenido.TabIndex = 3;
			this.imgContenido.TabStop = false;
			// 
			// frmAyuda
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.White;
			this.ClientSize = new System.Drawing.Size(742, 457);
			this.Controls.Add(this.imgContenido);
			this.Controls.Add(this.lblTitulo);
			this.Controls.Add(this.tvwContenidos);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmAyuda";
			this.Text = "Ayuda";
			this.Load += new System.EventHandler(this.Ayuda_Load);
			((System.ComponentModel.ISupportInitialize)(this.imgContenido)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion
		private System.Windows.Forms.Label lblTitulo;
		public System.Windows.Forms.TreeView tvwContenidos;
		private System.Windows.Forms.PictureBox imgContenido;
	}
}