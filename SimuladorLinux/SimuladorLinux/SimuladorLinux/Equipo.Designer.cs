﻿namespace SimuladorLinux
{
	partial class frmEquipo
	{
		/// <summary>
		/// Variable del diseñador requerida.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Limpiar los recursos que se estén utilizando.
		/// </summary>
		/// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Código generado por el Diseñador de Windows Forms

		/// <summary>
		/// Método necesario para admitir el Diseñador. No se puede modificar
		/// el contenido del método con el editor de código.
		/// </summary>
		private void InitializeComponent()
		{
			this.lblEstado = new System.Windows.Forms.Label();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.imgCargando = new System.Windows.Forms.PictureBox();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgCargando)).BeginInit();
			this.SuspendLayout();
			// 
			// lblEstado
			// 
			this.lblEstado.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.lblEstado.AutoSize = true;
			this.lblEstado.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblEstado.ForeColor = System.Drawing.Color.White;
			this.lblEstado.Location = new System.Drawing.Point(199, 256);
			this.lblEstado.Name = "lblEstado";
			this.lblEstado.Size = new System.Drawing.Size(150, 24);
			this.lblEstado.TabIndex = 1;
			this.lblEstado.Text = "Iniciando equipo";
			// 
			// pictureBox1
			// 
			this.pictureBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
			this.pictureBox1.Image = global::SimuladorLinux.Properties.Resources.linux_ico2;
			this.pictureBox1.Location = new System.Drawing.Point(222, 153);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(100, 100);
			this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.pictureBox1.TabIndex = 2;
			this.pictureBox1.TabStop = false;
			// 
			// imgCargando
			// 
			this.imgCargando.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.imgCargando.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.imgCargando.Image = global::SimuladorLinux.Properties.Resources.cargando;
			this.imgCargando.Location = new System.Drawing.Point(222, 372);
			this.imgCargando.Name = "imgCargando";
			this.imgCargando.Size = new System.Drawing.Size(100, 100);
			this.imgCargando.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.imgCargando.TabIndex = 0;
			this.imgCargando.TabStop = false;
			// 
			// frmEquipo
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.Black;
			this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.ClientSize = new System.Drawing.Size(544, 506);
			this.Controls.Add(this.lblEstado);
			this.Controls.Add(this.imgCargando);
			this.Controls.Add(this.pictureBox1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmEquipo";
			this.ShowIcon = false;
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.Text = "Iniciando equipo";
			this.Load += new System.EventHandler(this.Equipo_Load);
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgCargando)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.PictureBox imgCargando;
		private System.Windows.Forms.Label lblEstado;
		private System.Windows.Forms.PictureBox pictureBox1;
	}
}

