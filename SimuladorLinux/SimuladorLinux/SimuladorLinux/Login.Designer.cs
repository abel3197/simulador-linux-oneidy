﻿namespace SimuladorLinux
{
	partial class frmLogin
	{
		/// <summary>
		/// Variable del diseñador requerida.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Limpiar los recursos que se estén utilizando.
		/// </summary>
		/// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Código generado por el Diseñador de Windows Forms

		/// <summary>
		/// Método necesario para admitir el Diseñador. No se puede modificar
		/// el contenido del método con el editor de código.
		/// </summary>
		private void InitializeComponent()
		{
			this.txtContrasenia = new System.Windows.Forms.TextBox();
			this.txtUsuario = new System.Windows.Forms.TextBox();
			this.btnIniciarSesion = new System.Windows.Forms.Button();
			this.Label2 = new System.Windows.Forms.Label();
			this.Label1 = new System.Windows.Forms.Label();
			this.pictureBox3 = new System.Windows.Forms.PictureBox();
			this.pnlFormulario = new System.Windows.Forms.Panel();
			this.btnAyuda = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
			this.pnlFormulario.SuspendLayout();
			this.SuspendLayout();
			// 
			// txtContrasenia
			// 
			this.txtContrasenia.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.txtContrasenia.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(11)))), ((int)(((byte)(19)))));
			this.txtContrasenia.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.txtContrasenia.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtContrasenia.ForeColor = System.Drawing.Color.White;
			this.txtContrasenia.Location = new System.Drawing.Point(49, 136);
			this.txtContrasenia.Margin = new System.Windows.Forms.Padding(10);
			this.txtContrasenia.Name = "txtContrasenia";
			this.txtContrasenia.PasswordChar = '*';
			this.txtContrasenia.Size = new System.Drawing.Size(195, 19);
			this.txtContrasenia.TabIndex = 14;
			this.txtContrasenia.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// txtUsuario
			// 
			this.txtUsuario.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.txtUsuario.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(11)))), ((int)(((byte)(19)))));
			this.txtUsuario.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.txtUsuario.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtUsuario.ForeColor = System.Drawing.Color.White;
			this.txtUsuario.Location = new System.Drawing.Point(47, 76);
			this.txtUsuario.Margin = new System.Windows.Forms.Padding(10);
			this.txtUsuario.MaxLength = 20;
			this.txtUsuario.Name = "txtUsuario";
			this.txtUsuario.Size = new System.Drawing.Size(197, 19);
			this.txtUsuario.TabIndex = 1;
			this.txtUsuario.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// btnIniciarSesion
			// 
			this.btnIniciarSesion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnIniciarSesion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(11)))), ((int)(((byte)(19)))));
			this.btnIniciarSesion.FlatAppearance.BorderSize = 0;
			this.btnIniciarSesion.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(11)))), ((int)(((byte)(19)))));
			this.btnIniciarSesion.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(7)))), ((int)(((byte)(14)))));
			this.btnIniciarSesion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnIniciarSesion.Font = new System.Drawing.Font("Arial Black", 11.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnIniciarSesion.ForeColor = System.Drawing.Color.WhiteSmoke;
			this.btnIniciarSesion.Location = new System.Drawing.Point(47, 168);
			this.btnIniciarSesion.Name = "btnIniciarSesion";
			this.btnIniciarSesion.Size = new System.Drawing.Size(197, 40);
			this.btnIniciarSesion.TabIndex = 12;
			this.btnIniciarSesion.Text = "Iniciar sesión";
			this.btnIniciarSesion.UseVisualStyleBackColor = false;
			this.btnIniciarSesion.Click += new System.EventHandler(this.btnIniciarSesion_Click);
			// 
			// Label2
			// 
			this.Label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.Label2.AutoSize = true;
			this.Label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(7)))), ((int)(((byte)(15)))));
			this.Label2.Font = new System.Drawing.Font("Courier New", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Label2.ForeColor = System.Drawing.Color.White;
			this.Label2.Location = new System.Drawing.Point(46, 117);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(96, 16);
			this.Label2.TabIndex = 11;
			this.Label2.Text = "Contraseña:";
			// 
			// Label1
			// 
			this.Label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.Label1.AutoSize = true;
			this.Label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(7)))), ((int)(((byte)(15)))));
			this.Label1.Font = new System.Drawing.Font("Courier New", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Label1.ForeColor = System.Drawing.Color.White;
			this.Label1.Location = new System.Drawing.Point(46, 57);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(72, 16);
			this.Label1.TabIndex = 10;
			this.Label1.Text = "Usuario:";
			// 
			// pictureBox3
			// 
			this.pictureBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.pictureBox3.BackColor = System.Drawing.Color.Transparent;
			this.pictureBox3.Image = global::SimuladorLinux.Properties.Resources.marco_transparente5;
			this.pictureBox3.Location = new System.Drawing.Point(3, 17);
			this.pictureBox3.Name = "pictureBox3";
			this.pictureBox3.Size = new System.Drawing.Size(289, 226);
			this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.pictureBox3.TabIndex = 17;
			this.pictureBox3.TabStop = false;
			// 
			// pnlFormulario
			// 
			this.pnlFormulario.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.pnlFormulario.BackColor = System.Drawing.Color.Transparent;
			this.pnlFormulario.Controls.Add(this.txtContrasenia);
			this.pnlFormulario.Controls.Add(this.Label1);
			this.pnlFormulario.Controls.Add(this.txtUsuario);
			this.pnlFormulario.Controls.Add(this.Label2);
			this.pnlFormulario.Controls.Add(this.btnIniciarSesion);
			this.pnlFormulario.Controls.Add(this.pictureBox3);
			this.pnlFormulario.Location = new System.Drawing.Point(12, 204);
			this.pnlFormulario.Name = "pnlFormulario";
			this.pnlFormulario.Size = new System.Drawing.Size(297, 255);
			this.pnlFormulario.TabIndex = 18;
			// 
			// btnAyuda
			// 
			this.btnAyuda.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnAyuda.BackColor = System.Drawing.Color.Transparent;
			this.btnAyuda.BackgroundImage = global::SimuladorLinux.Properties.Resources.help_person;
			this.btnAyuda.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.btnAyuda.FlatAppearance.BorderSize = 0;
			this.btnAyuda.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnAyuda.Location = new System.Drawing.Point(820, 482);
			this.btnAyuda.Name = "btnAyuda";
			this.btnAyuda.Size = new System.Drawing.Size(30, 30);
			this.btnAyuda.TabIndex = 41;
			this.btnAyuda.UseVisualStyleBackColor = false;
			this.btnAyuda.Click += new System.EventHandler(this.btnAyuda_Click);
			// 
			// frmLogin
			// 
			this.AcceptButton = this.btnIniciarSesion;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackgroundImage = global::SimuladorLinux.Properties.Resources.login_background;
			this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.ClientSize = new System.Drawing.Size(854, 518);
			this.Controls.Add(this.btnAyuda);
			this.Controls.Add(this.pnlFormulario);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmLogin";
			this.ShowIcon = false;
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.Text = "Iniciar sesión";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmLogin_FormClosing);
			this.Load += new System.EventHandler(this.Login_Load);
			((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
			this.pnlFormulario.ResumeLayout(false);
			this.pnlFormulario.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		internal System.Windows.Forms.TextBox txtContrasenia;
		internal System.Windows.Forms.TextBox txtUsuario;
		internal System.Windows.Forms.Label Label2;
		internal System.Windows.Forms.Label Label1;
		public System.Windows.Forms.Button btnIniciarSesion;
		private System.Windows.Forms.PictureBox pictureBox3;
		private System.Windows.Forms.Panel pnlFormulario;
		private System.Windows.Forms.Button btnAyuda;
	}
}

