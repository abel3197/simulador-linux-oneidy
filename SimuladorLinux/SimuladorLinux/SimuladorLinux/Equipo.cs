﻿using System;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Linq;
using MySql.Data.MySqlClient;

namespace SimuladorLinux
{
	public partial class frmEquipo : Form
	{
		static public frmEquipo equipo;
		string estado;
		public frmEquipo(string estado = "iniciando")
		{
			InitializeComponent();
			equipo = this;
			frmContenedor.ConfigurarMdiParent(this);
			this.estado = estado;
			if (estado == "apagando")
			{
				this.Text = this.lblEstado.Text = "Apagando equipo";
			}
		}

		private void Equipo_Load(object sender, EventArgs e)
		{
			if (this.MdiParent != null)
			{
				MdiClient mdiClient = this.MdiParent.Controls.OfType<MdiClient>().FirstOrDefault();
				Size tamañoMDI = new Size(mdiClient.Size.Width /*- 5*/, mdiClient.Size.Height /*- 5*/);
				this.Size = tamañoMDI;
			}
			CerrarEn3Segundos();
		}

		Timer tmr;
		public void CerrarEn3Segundos()
		{
			tmr = new Timer();
			tmr.Tick += delegate
			{
				this.Close();
				if (estado == "apagando") {
					frmContenedor.contenedor.Close();
				}
			};
			tmr.Interval = (int)TimeSpan.FromSeconds(3).TotalMilliseconds;
			tmr.Start();
		}
	}
}

