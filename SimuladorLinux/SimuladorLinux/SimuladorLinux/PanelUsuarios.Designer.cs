﻿namespace SimuladorLinux
{
	partial class frmPanelUsuarios
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPanelUsuarios));
			this.lstUsuarios = new System.Windows.Forms.ListView();
			this.colId = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.colNombre = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.colUsuario = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.colContrasenia = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.colTipo = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.btnNuevo = new System.Windows.Forms.Button();
			this.btnCambiarFondo = new System.Windows.Forms.Button();
			this.btnAyuda = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// lstUsuarios
			// 
			this.lstUsuarios.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.lstUsuarios.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colId,
            this.colUsuario,
            this.colNombre,
            this.colContrasenia,
            this.colTipo});
			this.lstUsuarios.FullRowSelect = true;
			this.lstUsuarios.HideSelection = false;
			this.lstUsuarios.Location = new System.Drawing.Point(12, 41);
			this.lstUsuarios.MultiSelect = false;
			this.lstUsuarios.Name = "lstUsuarios";
			this.lstUsuarios.Size = new System.Drawing.Size(543, 230);
			this.lstUsuarios.TabIndex = 0;
			this.lstUsuarios.UseCompatibleStateImageBehavior = false;
			this.lstUsuarios.View = System.Windows.Forms.View.Details;
			this.lstUsuarios.SelectedIndexChanged += new System.EventHandler(this.lstUsuarios_SelectedIndexChanged);
			this.lstUsuarios.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lstUsuarios_MouseDoubleClick);
			// 
			// colId
			// 
			this.colId.Text = "ID";
			this.colId.Width = 40;
			// 
			// colNombre
			// 
			this.colNombre.DisplayIndex = 1;
			this.colNombre.Text = "Nombre";
			this.colNombre.Width = 120;
			// 
			// colUsuario
			// 
			this.colUsuario.DisplayIndex = 2;
			this.colUsuario.Text = "Usuario";
			this.colUsuario.Width = 100;
			// 
			// colContrasenia
			// 
			this.colContrasenia.Text = "Contraseña";
			this.colContrasenia.Width = 100;
			// 
			// colTipo
			// 
			this.colTipo.Text = "Tipo";
			this.colTipo.Width = 100;
			// 
			// btnNuevo
			// 
			this.btnNuevo.Location = new System.Drawing.Point(12, 12);
			this.btnNuevo.Name = "btnNuevo";
			this.btnNuevo.Size = new System.Drawing.Size(76, 23);
			this.btnNuevo.TabIndex = 1;
			this.btnNuevo.Text = "Nuevo";
			this.btnNuevo.UseVisualStyleBackColor = true;
			this.btnNuevo.Click += new System.EventHandler(this.btnNuevo_Click);
			// 
			// btnCambiarFondo
			// 
			this.btnCambiarFondo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnCambiarFondo.Location = new System.Drawing.Point(333, 12);
			this.btnCambiarFondo.Name = "btnCambiarFondo";
			this.btnCambiarFondo.Size = new System.Drawing.Size(185, 23);
			this.btnCambiarFondo.TabIndex = 2;
			this.btnCambiarFondo.Text = "Cambiar tu fondo de escritorio";
			this.btnCambiarFondo.UseVisualStyleBackColor = true;
			this.btnCambiarFondo.Click += new System.EventHandler(this.btnCambiarFondo_Click);
			// 
			// btnAyuda
			// 
			this.btnAyuda.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnAyuda.BackColor = System.Drawing.Color.Transparent;
			this.btnAyuda.BackgroundImage = global::SimuladorLinux.Properties.Resources.help_person;
			this.btnAyuda.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.btnAyuda.FlatAppearance.BorderSize = 0;
			this.btnAyuda.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnAyuda.Location = new System.Drawing.Point(524, 5);
			this.btnAyuda.Name = "btnAyuda";
			this.btnAyuda.Size = new System.Drawing.Size(30, 30);
			this.btnAyuda.TabIndex = 41;
			this.btnAyuda.UseVisualStyleBackColor = false;
			this.btnAyuda.Click += new System.EventHandler(this.btnAyuda_Click);
			// 
			// frmPanelUsuarios
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(567, 283);
			this.Controls.Add(this.btnAyuda);
			this.Controls.Add(this.btnCambiarFondo);
			this.Controls.Add(this.btnNuevo);
			this.Controls.Add(this.lstUsuarios);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "frmPanelUsuarios";
			this.Text = "Panel de usuarios";
			this.Load += new System.EventHandler(this.frmRegistroUsuarios_Load);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.ListView lstUsuarios;
		private System.Windows.Forms.ColumnHeader colId;
		private System.Windows.Forms.ColumnHeader colNombre;
		private System.Windows.Forms.ColumnHeader colUsuario;
		private System.Windows.Forms.ColumnHeader colContrasenia;
		private System.Windows.Forms.ColumnHeader colTipo;
		private System.Windows.Forms.Button btnNuevo;
		private System.Windows.Forms.Button btnCambiarFondo;
		private System.Windows.Forms.Button btnAyuda;
	}
}