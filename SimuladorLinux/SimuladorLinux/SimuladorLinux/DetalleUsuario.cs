﻿using MySql.Data.MySqlClient;
using System;
using System.Configuration;
using System.Data;
using System.Windows.Forms;

namespace SimuladorLinux
{
	public partial class frmDetalleUsuario : Form
	{
		public Action CargarUsuarios;

		public frmDetalleUsuario()
		{
			InitializeComponent();
			//this.MdiParent = frmContenedor.contenedor;
		}
		public frmDetalleUsuario(string id, string usuario, string nombre, string contrasenia, string tipo) : this()
		{
			this.campoId.Text = id;
			this.campoNombre.Text = nombre;
			this.campoUsuario.Text = usuario;
			this.campoContrasenia.Text = contrasenia;
			this.campoTipo.Text = tipo;

			this.btnGuardar.Text = "Guardar";
			this.btnEliminar.Visible = true;
			this.campoUsuario.Enabled = false;
			this.campoTipo.Enabled = false;
			if (usuario == "admin" || usuario == "guest")
			{
				this.btnEliminar.Visible = false;
				this.campoNombre.Enabled = false;
				if (usuario == "guest")
				{
					this.campoContrasenia.Enabled = false;
				}
			}
			else if (Usuario.actual.tipo == "Administrador")
			{
				this.campoTipo.Enabled = true;
			}
		}

		private void btnCancelar_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void frmDetalleUsuario_Load(object sender, EventArgs e)
		{

		}

		private void btnGuardar_Click(object sender, EventArgs e)
		{
			string nombre = campoNombre.Text;
			if (String.IsNullOrWhiteSpace(nombre))
			{
				MessageBox.Show("Debe especificar un valor en el campo nombre.", "Nombre inválido", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			string usuario = campoUsuario.Text;
			if (String.IsNullOrWhiteSpace(usuario))
			{
				MessageBox.Show("Debe especificar un valor en el campo usuario.", "Usuario inválido", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			string contrasenia = campoContrasenia.Text;
			if (String.IsNullOrWhiteSpace(contrasenia))
			{
				MessageBox.Show("Debe especificar un valor en el campo contraseña.", "Contraseña inválida", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			string tipo = campoTipo.Text;
			if (String.IsNullOrWhiteSpace(tipo))
			{
				MessageBox.Show("Debe especificar un valor en el campo tipo.", "Tipo inválido", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}

			string connectionString = ConfigurationManager.ConnectionStrings["mysqlConnection"].ConnectionString;
			MySqlConnection con = new MySqlConnection(connectionString);
			try
			{
				MySqlCommand cmd = new MySqlCommand();
				cmd.Connection = con;
				cmd.Parameters.AddWithValue("@Id", campoId.Text);
				cmd.Parameters.AddWithValue("@Nombre", campoNombre.Text);
				cmd.Parameters.AddWithValue("@Usuario", campoUsuario.Text);
				cmd.Parameters.AddWithValue("@Contrasenia", campoContrasenia.Text);
				cmd.Parameters.AddWithValue("@Tipo", campoTipo.Text);
				con.Open();
				bool campoIdNoTieneValor = String.IsNullOrWhiteSpace(campoId.Text);
				if (campoIdNoTieneValor)
				{
					cmd.CommandText = "Insert Usuarios(Nombre, Usuario, Contrasenia, Tipo) " +
						"Values (@Nombre, @Usuario, @Contrasenia, @Tipo);\n" +
						"select LAST_INSERT_ID();";
					object id = cmd.ExecuteScalar();
					this.campoId.Text = id == null ? null : id.ToString();
					frmExplorador.VerificarCrearCarpetaUsuario(campoUsuario.Text);
					MessageBox.Show("Se ha insertado el registro " + id, "Agregado", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				else
				{
					cmd.CommandText = "Update Usuarios set" +
						" Nombre=@Nombre," +
						" Usuario=@Usuario," +
						" Contrasenia=@Contrasenia," +
						" Tipo=@Tipo " +
						"Where Id=@Id";
					cmd.ExecuteNonQuery();
					MessageBox.Show("Se ha actualizado el registro " + campoId.Text, "Actualizado", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				this.Close();
				if (CargarUsuarios != null) CargarUsuarios.Invoke();
			}
			catch (Exception ex)
			{
				MessageBox.Show("Ha ocurrido un insertando registro: \n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
			finally
			{
				if (con.State != ConnectionState.Closed)
				{
					con.Close();
				}
			}
		}

		private void btnEliminar_Click(object sender, EventArgs e)
		{
			string nombre = campoNombre.Text;
			DialogResult opcion = MessageBox.Show(
				"¿Está seguro que desea eliminar el registro" +
				(String.IsNullOrWhiteSpace(nombre) ? "" : " " + nombre) + "?",
				"Eliminar", MessageBoxButtons.OKCancel, MessageBoxIcon.Question
			);
			if (opcion == DialogResult.OK)
			{
				string connectionString = ConfigurationManager.ConnectionStrings["mysqlConnection"].ConnectionString;
				MySqlConnection con = new MySqlConnection(connectionString);
				try
				{
					MySqlCommand cmd = new MySqlCommand("Delete from Usuarios Where Id = @Id", con);
					cmd.Parameters.AddWithValue("@Id", campoId.Text);

					con.Open();
					cmd.ExecuteNonQuery();

					MessageBox.Show("Se ha eliminado el registro el registro " + campoId.Text, "Eliminado", MessageBoxButtons.OK, MessageBoxIcon.Information);
					this.Close();
					if (CargarUsuarios != null) CargarUsuarios.Invoke();
				}
				catch (Exception ex)
				{
					MessageBox.Show("Ha ocurrido un eliminando registro: \n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				}
				finally
				{
					if (con.State != ConnectionState.Closed)
					{
						con.Close();
					}
				}
			}
		}

		private void btnMostrarContrasenia_Click(object sender, EventArgs e)
		{
			if (campoContrasenia.PasswordChar == '*')
			{
				campoContrasenia.PasswordChar = Char.MinValue;
				btnMostrarContrasenia.Image = null;
			}
			else {
				campoContrasenia.PasswordChar = '*';
				btnMostrarContrasenia.Image = Properties.Resources.marco_transparente5;
			}
		}
	}
}
