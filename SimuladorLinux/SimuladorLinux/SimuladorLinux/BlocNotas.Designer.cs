﻿namespace SimuladorLinux
{
	partial class frmBlocNotas
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBlocNotas));
			this.ToolStrip1 = new System.Windows.Forms.ToolStrip();
			this.ToolStripSplitButton2 = new System.Windows.Forms.ToolStripDropDownButton();
			this.NuevoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.AbrirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.GuardarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.SalirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.ToolStripSplitButton1 = new System.Windows.Forms.ToolStripDropDownButton();
			this.CopiarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.PegarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.CortarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.DeshacerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.RehacerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.SelecionarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.DeselecionarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.ToolStripSplitButton3 = new System.Windows.Forms.ToolStripDropDownButton();
			this.FuenteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.ajusteDeLineaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.txtContenido = new System.Windows.Forms.RichTextBox();
			this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.tsbAyuda = new System.Windows.Forms.ToolStripButton();
			this.ToolStrip1.SuspendLayout();
			this.SuspendLayout();
			// 
			// ToolStrip1
			// 
			this.ToolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripSplitButton2,
            this.ToolStripSplitButton1,
            this.ToolStripSplitButton3,
            this.toolStripSeparator1,
            this.tsbAyuda});
			this.ToolStrip1.Location = new System.Drawing.Point(0, 0);
			this.ToolStrip1.Name = "ToolStrip1";
			this.ToolStrip1.Size = new System.Drawing.Size(766, 25);
			this.ToolStrip1.TabIndex = 2;
			this.ToolStrip1.Text = "ToolStrip1";
			// 
			// ToolStripSplitButton2
			// 
			this.ToolStripSplitButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.ToolStripSplitButton2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.NuevoToolStripMenuItem,
            this.AbrirToolStripMenuItem,
            this.GuardarToolStripMenuItem,
            this.SalirToolStripMenuItem});
			this.ToolStripSplitButton2.Image = ((System.Drawing.Image)(resources.GetObject("ToolStripSplitButton2.Image")));
			this.ToolStripSplitButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.ToolStripSplitButton2.Name = "ToolStripSplitButton2";
			this.ToolStripSplitButton2.Size = new System.Drawing.Size(61, 22);
			this.ToolStripSplitButton2.Text = "Archivo";
			// 
			// NuevoToolStripMenuItem
			// 
			this.NuevoToolStripMenuItem.Image = global::SimuladorLinux.Properties.Resources.nuevo;
			this.NuevoToolStripMenuItem.Name = "NuevoToolStripMenuItem";
			this.NuevoToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
			this.NuevoToolStripMenuItem.Text = "Nuevo";
			this.NuevoToolStripMenuItem.Click += new System.EventHandler(this.NuevoToolStripMenuItem_Click);
			// 
			// AbrirToolStripMenuItem
			// 
			this.AbrirToolStripMenuItem.Image = global::SimuladorLinux.Properties.Resources.abrir;
			this.AbrirToolStripMenuItem.Name = "AbrirToolStripMenuItem";
			this.AbrirToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
			this.AbrirToolStripMenuItem.Text = "Abrir";
			this.AbrirToolStripMenuItem.Click += new System.EventHandler(this.AbrirToolStripMenuItem_Click);
			// 
			// GuardarToolStripMenuItem
			// 
			this.GuardarToolStripMenuItem.Image = global::SimuladorLinux.Properties.Resources.guardar;
			this.GuardarToolStripMenuItem.Name = "GuardarToolStripMenuItem";
			this.GuardarToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
			this.GuardarToolStripMenuItem.Text = "Guardar";
			this.GuardarToolStripMenuItem.Click += new System.EventHandler(this.GuardarToolStripMenuItem_Click);
			// 
			// SalirToolStripMenuItem
			// 
			this.SalirToolStripMenuItem.Name = "SalirToolStripMenuItem";
			this.SalirToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
			this.SalirToolStripMenuItem.Text = "Salir";
			this.SalirToolStripMenuItem.Click += new System.EventHandler(this.SalirToolStripMenuItem_Click);
			// 
			// ToolStripSplitButton1
			// 
			this.ToolStripSplitButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.ToolStripSplitButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CopiarToolStripMenuItem,
            this.PegarToolStripMenuItem,
            this.CortarToolStripMenuItem,
            this.DeshacerToolStripMenuItem,
            this.RehacerToolStripMenuItem,
            this.SelecionarToolStripMenuItem,
            this.DeselecionarToolStripMenuItem});
			this.ToolStripSplitButton1.Image = ((System.Drawing.Image)(resources.GetObject("ToolStripSplitButton1.Image")));
			this.ToolStripSplitButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.ToolStripSplitButton1.Name = "ToolStripSplitButton1";
			this.ToolStripSplitButton1.Size = new System.Drawing.Size(50, 22);
			this.ToolStripSplitButton1.Text = "Editar";
			// 
			// CopiarToolStripMenuItem
			// 
			this.CopiarToolStripMenuItem.Image = global::SimuladorLinux.Properties.Resources.copiar;
			this.CopiarToolStripMenuItem.Name = "CopiarToolStripMenuItem";
			this.CopiarToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
			this.CopiarToolStripMenuItem.Text = "Copiar";
			this.CopiarToolStripMenuItem.Click += new System.EventHandler(this.CopiarToolStripMenuItem_Click);
			// 
			// PegarToolStripMenuItem
			// 
			this.PegarToolStripMenuItem.Image = global::SimuladorLinux.Properties.Resources.pegar;
			this.PegarToolStripMenuItem.Name = "PegarToolStripMenuItem";
			this.PegarToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
			this.PegarToolStripMenuItem.Text = "Pegar";
			this.PegarToolStripMenuItem.Click += new System.EventHandler(this.PegarToolStripMenuItem_Click);
			// 
			// CortarToolStripMenuItem
			// 
			this.CortarToolStripMenuItem.Image = global::SimuladorLinux.Properties.Resources.cortar;
			this.CortarToolStripMenuItem.Name = "CortarToolStripMenuItem";
			this.CortarToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
			this.CortarToolStripMenuItem.Text = "Cortar";
			this.CortarToolStripMenuItem.Click += new System.EventHandler(this.CortarToolStripMenuItem_Click);
			// 
			// DeshacerToolStripMenuItem
			// 
			this.DeshacerToolStripMenuItem.Name = "DeshacerToolStripMenuItem";
			this.DeshacerToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
			this.DeshacerToolStripMenuItem.Text = "Deshacer";
			this.DeshacerToolStripMenuItem.Click += new System.EventHandler(this.DeshacerToolStripMenuItem_Click);
			// 
			// RehacerToolStripMenuItem
			// 
			this.RehacerToolStripMenuItem.Name = "RehacerToolStripMenuItem";
			this.RehacerToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
			this.RehacerToolStripMenuItem.Text = "Rehacer";
			this.RehacerToolStripMenuItem.Click += new System.EventHandler(this.RehacerToolStripMenuItem_Click);
			// 
			// SelecionarToolStripMenuItem
			// 
			this.SelecionarToolStripMenuItem.Name = "SelecionarToolStripMenuItem";
			this.SelecionarToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
			this.SelecionarToolStripMenuItem.Text = "Selecionar Todo";
			this.SelecionarToolStripMenuItem.Click += new System.EventHandler(this.SelecionarToolStripMenuItem_Click);
			// 
			// DeselecionarToolStripMenuItem
			// 
			this.DeselecionarToolStripMenuItem.Name = "DeselecionarToolStripMenuItem";
			this.DeselecionarToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
			this.DeselecionarToolStripMenuItem.Text = "Deselecionar";
			this.DeselecionarToolStripMenuItem.Click += new System.EventHandler(this.DeselecionarToolStripMenuItem_Click);
			// 
			// ToolStripSplitButton3
			// 
			this.ToolStripSplitButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.ToolStripSplitButton3.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.FuenteToolStripMenuItem,
            this.ajusteDeLineaToolStripMenuItem});
			this.ToolStripSplitButton3.Image = ((System.Drawing.Image)(resources.GetObject("ToolStripSplitButton3.Image")));
			this.ToolStripSplitButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.ToolStripSplitButton3.Name = "ToolStripSplitButton3";
			this.ToolStripSplitButton3.Size = new System.Drawing.Size(65, 22);
			this.ToolStripSplitButton3.Text = "Formato";
			// 
			// FuenteToolStripMenuItem
			// 
			this.FuenteToolStripMenuItem.Name = "FuenteToolStripMenuItem";
			this.FuenteToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
			this.FuenteToolStripMenuItem.Text = "Fuente";
			this.FuenteToolStripMenuItem.Click += new System.EventHandler(this.FuenteToolStripMenuItem_Click);
			// 
			// ajusteDeLineaToolStripMenuItem
			// 
			this.ajusteDeLineaToolStripMenuItem.Checked = true;
			this.ajusteDeLineaToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
			this.ajusteDeLineaToolStripMenuItem.Name = "ajusteDeLineaToolStripMenuItem";
			this.ajusteDeLineaToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
			this.ajusteDeLineaToolStripMenuItem.Text = "Ajuste de linea";
			this.ajusteDeLineaToolStripMenuItem.Click += new System.EventHandler(this.AjusteDeLineaToolStripMenuItem_Click);
			// 
			// txtContenido
			// 
			this.txtContenido.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.txtContenido.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.txtContenido.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtContenido.Location = new System.Drawing.Point(0, 29);
			this.txtContenido.Margin = new System.Windows.Forms.Padding(4);
			this.txtContenido.Name = "txtContenido";
			this.txtContenido.Size = new System.Drawing.Size(766, 403);
			this.txtContenido.TabIndex = 3;
			this.txtContenido.Text = "";
			this.txtContenido.TextChanged += new System.EventHandler(this.txtContenido_TextChanged);
			// 
			// toolStripSeparator1
			// 
			this.toolStripSeparator1.Name = "toolStripSeparator1";
			this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
			// 
			// tsbAyuda
			// 
			this.tsbAyuda.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.tsbAyuda.Image = global::SimuladorLinux.Properties.Resources.help_person;
			this.tsbAyuda.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.tsbAyuda.Name = "tsbAyuda";
			this.tsbAyuda.Size = new System.Drawing.Size(23, 22);
			this.tsbAyuda.Text = "Ayuda";
			this.tsbAyuda.Click += new System.EventHandler(this.tsbAyuda_Click);
			// 
			// frmBlocNotas
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(766, 432);
			this.Controls.Add(this.ToolStrip1);
			this.Controls.Add(this.txtContenido);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "frmBlocNotas";
			this.Text = "Editor de texto";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmBlocNotas_FormClosing);
			this.Load += new System.EventHandler(this.frmBlocNotas_Load);
			this.ToolStrip1.ResumeLayout(false);
			this.ToolStrip1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion
		internal System.Windows.Forms.ToolStrip ToolStrip1;
		public System.Windows.Forms.RichTextBox txtContenido;
		internal System.Windows.Forms.ToolStripDropDownButton ToolStripSplitButton2;
		internal System.Windows.Forms.ToolStripMenuItem NuevoToolStripMenuItem;
		internal System.Windows.Forms.ToolStripMenuItem AbrirToolStripMenuItem;
		internal System.Windows.Forms.ToolStripMenuItem GuardarToolStripMenuItem;
		internal System.Windows.Forms.ToolStripMenuItem SalirToolStripMenuItem;
		internal System.Windows.Forms.ToolStripDropDownButton ToolStripSplitButton1;
		internal System.Windows.Forms.ToolStripMenuItem CopiarToolStripMenuItem;
		internal System.Windows.Forms.ToolStripMenuItem PegarToolStripMenuItem;
		internal System.Windows.Forms.ToolStripMenuItem CortarToolStripMenuItem;
		internal System.Windows.Forms.ToolStripMenuItem DeshacerToolStripMenuItem;
		internal System.Windows.Forms.ToolStripMenuItem RehacerToolStripMenuItem;
		internal System.Windows.Forms.ToolStripMenuItem SelecionarToolStripMenuItem;
		internal System.Windows.Forms.ToolStripMenuItem DeselecionarToolStripMenuItem;
		internal System.Windows.Forms.ToolStripDropDownButton ToolStripSplitButton3;
		internal System.Windows.Forms.ToolStripMenuItem FuenteToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem ajusteDeLineaToolStripMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
		private System.Windows.Forms.ToolStripButton tsbAyuda;
	}
}