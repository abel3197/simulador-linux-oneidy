﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SimuladorLinux
{
	static class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main()
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			Application.Run(new frmContenedor());
		}
		/*
			Application.Run(new frmAyuda());
			Application.Run(new frmBlocNotas());
			Application.Run(new frmEquipo());
			Application.Run(new frmEscritorio());
			Application.Run(new frmExplorador());
			Application.Run(new frmRegistroUsuarios());
			Application.Run(new frmReproductor());
		 */
	}
}
