﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace SimuladorLinux
{
	class IconoEscritorio
	{
		TableLayoutPanel tlpIconoEscritorio;
		PictureBox imgTipoIcono;
		Label lblTextoIcono;

		static public List<string> extAudios = new List<string>() { ".mp3", ".mid", ".midi", ".wav", ".acc", ".ac3", ".flac" };
		static public List<string> extImagenes = new List<string>() { ".png", ".bpm", ".jpg", ".jpeg", ".ico", ".gif", ".tif", ".tiff" };
		static public List<string> extVideos = new List<string>() { ".mp4", ".mpg", ".mov", ".wmv", ".avi", ".dvd", ".mpe", ".mpeg" };
		static public List<string> extComprimidos = new List<string>() { ".zip", ".rar", ".7z" };

		public IconoEscritorio()
		{
			this.tlpIconoEscritorio = new TableLayoutPanel();
			this.imgTipoIcono = new PictureBox();
			this.lblTextoIcono = new Label();
			EventHandler mouseEnter = (sender, e) =>
			{
				this.tlpIconoEscritorio.BackgroundImage = Properties.Resources.marco_transparente7;
			};
			EventHandler mouseLeave = (sender, e) =>
			{
				this.tlpIconoEscritorio.BackgroundImage = null;
			};
			//this.tlpIconoEscritorio.SuspendLayout();
			//((System.ComponentModel.ISupportInitialize)(this.imgTipoIcono)).BeginInit();
			// 
			// tlpIconoEscritorio
			// 
			this.tlpIconoEscritorio.ColumnCount = 1;
			this.tlpIconoEscritorio.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
			this.tlpIconoEscritorio.Controls.Add(this.imgTipoIcono, 0, 0);
			this.tlpIconoEscritorio.Controls.Add(this.lblTextoIcono, 0, 1);
			//this.tlpIconoEscritorio.Location = new Point(3, 3);
			this.tlpIconoEscritorio.Name = "tlpIconoEscritorio";
			this.tlpIconoEscritorio.RowCount = 2;
			this.tlpIconoEscritorio.RowStyles.Add(new RowStyle(SizeType.Percent, 60F));
			this.tlpIconoEscritorio.RowStyles.Add(new RowStyle(SizeType.Percent, 40F));
			this.tlpIconoEscritorio.Size = new Size(87, 96);
			//this.tlpIconoEscritorio.TabIndex = 0;
			this.tlpIconoEscritorio.Cursor = Cursors.Hand;
			this.tlpIconoEscritorio.BackgroundImageLayout = ImageLayout.Stretch;
			this.tlpIconoEscritorio.MouseEnter += mouseEnter;
			this.tlpIconoEscritorio.MouseLeave += mouseLeave;

			// 
			// imgTipoIcono
			// 
			this.imgTipoIcono.Dock = DockStyle.Fill;
			this.imgTipoIcono.Image = Properties.Resources.file_sad;
			//this.imgTipoIcono.Location = new Point(3, 3);
			this.imgTipoIcono.Name = "imgTipoIcono";
			//this.imgTipoIcono.Size = new Size(81, 51);
			this.imgTipoIcono.SizeMode = PictureBoxSizeMode.Zoom;
			//this.imgTipoIcono.TabIndex = 0;
			//this.imgTipoIcono.TabStop = false;
			this.imgTipoIcono.MouseEnter += mouseEnter;
			this.imgTipoIcono.MouseLeave += mouseLeave;

			// 
			// lblTextoIcono
			// 
			this.lblTextoIcono.AutoEllipsis = true;
			this.lblTextoIcono.Dock = DockStyle.Fill;
			this.lblTextoIcono.Font = new Font("Verdana", 8.25F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0)));
			this.lblTextoIcono.ForeColor = Color.White;
			//this.lblTextoIcono.Location = new Point(3, 57);
			this.lblTextoIcono.Name = "lblTextoIcono";
			//this.lblTextoIcono.Size = new Size(81, 39);
			//this.lblTextoIcono.TabIndex = 1;
			this.lblTextoIcono.Text = "Icono";
			this.lblTextoIcono.TextAlign = ContentAlignment.MiddleCenter;
			this.lblTextoIcono.MouseEnter += mouseEnter;
			this.lblTextoIcono.MouseLeave += mouseLeave;
			//this.tlpIconoEscritorio.ResumeLayout(false);
			//((System.ComponentModel.ISupportInitialize)(this.imgTipoIcono)).EndInit();

		}

		public IconoEscritorio(DirectoryInfo info) : this()
		{
			lblTextoIcono.Text = info.Name;
			imgTipoIcono.Image = Properties.Resources.folder;
		}
		public IconoEscritorio(FileInfo info) : this()
		{
			lblTextoIcono.Text = info.Name;
			string extension = info.Extension.ToLower();
			if (extension == ".txt")
			{
				imgTipoIcono.Image = Properties.Resources.note_ico;
			}
			else if (extension == ".pdf")
			{
				imgTipoIcono.Image = Properties.Resources.pdf_icon;
			}
			else if (extAudios.Contains(extension))
			{
				imgTipoIcono.Image = Properties.Resources.folder;
			}
			else if (extComprimidos.Contains(extension))
			{
				imgTipoIcono.Image = Properties.Resources.folder;
			}
			else if (extImagenes.Contains(info.Extension.ToLower()))
			{
				imgTipoIcono.Image = Image.FromFile(info.FullName);
			}
			else if (extVideos.Contains(extension))
			{
				imgTipoIcono.Image = Properties.Resources.video_ico;
			}
		}

		public Control Control
		{
			get
			{
				return tlpIconoEscritorio;
			}
		}
	}
}
