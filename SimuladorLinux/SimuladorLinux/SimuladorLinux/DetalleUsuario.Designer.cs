﻿namespace SimuladorLinux
{
	partial class frmDetalleUsuario
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDetalleUsuario));
			this.btnGuardar = new System.Windows.Forms.Button();
			this.btnCancelar = new System.Windows.Forms.Button();
			this.lblId = new System.Windows.Forms.Label();
			this.campoId = new System.Windows.Forms.TextBox();
			this.campoNombre = new System.Windows.Forms.TextBox();
			this.lblNombre = new System.Windows.Forms.Label();
			this.campoUsuario = new System.Windows.Forms.TextBox();
			this.lblUsuario = new System.Windows.Forms.Label();
			this.campoContrasenia = new System.Windows.Forms.TextBox();
			this.lblContrasenia = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.campoTipo = new System.Windows.Forms.ComboBox();
			this.btnEliminar = new System.Windows.Forms.Button();
			this.btnMostrarContrasenia = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// btnGuardar
			// 
			this.btnGuardar.Location = new System.Drawing.Point(22, 199);
			this.btnGuardar.Name = "btnGuardar";
			this.btnGuardar.Size = new System.Drawing.Size(75, 23);
			this.btnGuardar.TabIndex = 11;
			this.btnGuardar.Text = "Agregar";
			this.btnGuardar.UseVisualStyleBackColor = true;
			this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
			// 
			// btnCancelar
			// 
			this.btnCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancelar.Location = new System.Drawing.Point(184, 199);
			this.btnCancelar.Name = "btnCancelar";
			this.btnCancelar.Size = new System.Drawing.Size(75, 23);
			this.btnCancelar.TabIndex = 1;
			this.btnCancelar.Text = "Cancelar";
			this.btnCancelar.UseVisualStyleBackColor = true;
			this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
			// 
			// lblId
			// 
			this.lblId.AutoSize = true;
			this.lblId.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblId.Location = new System.Drawing.Point(12, 16);
			this.lblId.Name = "lblId";
			this.lblId.Size = new System.Drawing.Size(23, 17);
			this.lblId.TabIndex = 2;
			this.lblId.Text = "Id:";
			// 
			// campoId
			// 
			this.campoId.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.campoId.Enabled = false;
			this.campoId.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.campoId.Location = new System.Drawing.Point(100, 13);
			this.campoId.Name = "campoId";
			this.campoId.Size = new System.Drawing.Size(172, 26);
			this.campoId.TabIndex = 3;
			// 
			// campoNombre
			// 
			this.campoNombre.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.campoNombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.campoNombre.Location = new System.Drawing.Point(100, 77);
			this.campoNombre.Name = "campoNombre";
			this.campoNombre.Size = new System.Drawing.Size(172, 26);
			this.campoNombre.TabIndex = 7;
			// 
			// lblNombre
			// 
			this.lblNombre.AutoSize = true;
			this.lblNombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblNombre.Location = new System.Drawing.Point(12, 80);
			this.lblNombre.Name = "lblNombre";
			this.lblNombre.Size = new System.Drawing.Size(62, 17);
			this.lblNombre.TabIndex = 4;
			this.lblNombre.Text = "Nombre:";
			// 
			// campoUsuario
			// 
			this.campoUsuario.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.campoUsuario.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.campoUsuario.Location = new System.Drawing.Point(100, 45);
			this.campoUsuario.Name = "campoUsuario";
			this.campoUsuario.Size = new System.Drawing.Size(172, 26);
			this.campoUsuario.TabIndex = 5;
			// 
			// lblUsuario
			// 
			this.lblUsuario.AutoSize = true;
			this.lblUsuario.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblUsuario.Location = new System.Drawing.Point(12, 48);
			this.lblUsuario.Name = "lblUsuario";
			this.lblUsuario.Size = new System.Drawing.Size(61, 17);
			this.lblUsuario.TabIndex = 6;
			this.lblUsuario.Text = "Usuario:";
			// 
			// campoContrasenia
			// 
			this.campoContrasenia.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.campoContrasenia.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.campoContrasenia.Location = new System.Drawing.Point(100, 109);
			this.campoContrasenia.Name = "campoContrasenia";
			this.campoContrasenia.PasswordChar = '*';
			this.campoContrasenia.Size = new System.Drawing.Size(141, 26);
			this.campoContrasenia.TabIndex = 8;
			// 
			// lblContrasenia
			// 
			this.lblContrasenia.AutoSize = true;
			this.lblContrasenia.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblContrasenia.Location = new System.Drawing.Point(12, 112);
			this.lblContrasenia.Name = "lblContrasenia";
			this.lblContrasenia.Size = new System.Drawing.Size(85, 17);
			this.lblContrasenia.TabIndex = 8;
			this.lblContrasenia.Text = "Contraseña:";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label5.Location = new System.Drawing.Point(12, 144);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(40, 17);
			this.label5.TabIndex = 10;
			this.label5.Text = "Tipo:";
			// 
			// campoTipo
			// 
			this.campoTipo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.campoTipo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.campoTipo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.campoTipo.FormattingEnabled = true;
			this.campoTipo.Items.AddRange(new object[] {
            "Administrador",
            "Estándar",
            "Invitado"});
			this.campoTipo.Location = new System.Drawing.Point(100, 142);
			this.campoTipo.Name = "campoTipo";
			this.campoTipo.Size = new System.Drawing.Size(172, 28);
			this.campoTipo.TabIndex = 11;
			// 
			// btnEliminar
			// 
			this.btnEliminar.Location = new System.Drawing.Point(103, 199);
			this.btnEliminar.Name = "btnEliminar";
			this.btnEliminar.Size = new System.Drawing.Size(75, 23);
			this.btnEliminar.TabIndex = 12;
			this.btnEliminar.Text = "Eliminar";
			this.btnEliminar.UseVisualStyleBackColor = true;
			this.btnEliminar.Visible = false;
			this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
			// 
			// btnMostrarContrasenia
			// 
			this.btnMostrarContrasenia.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnMostrarContrasenia.BackColor = System.Drawing.Color.Transparent;
			this.btnMostrarContrasenia.BackgroundImage = global::SimuladorLinux.Properties.Resources.ojo;
			this.btnMostrarContrasenia.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.btnMostrarContrasenia.FlatAppearance.BorderSize = 0;
			this.btnMostrarContrasenia.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnMostrarContrasenia.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnMostrarContrasenia.ForeColor = System.Drawing.Color.Red;
			this.btnMostrarContrasenia.Image = global::SimuladorLinux.Properties.Resources.marco_transparente5;
			this.btnMostrarContrasenia.Location = new System.Drawing.Point(247, 109);
			this.btnMostrarContrasenia.Name = "btnMostrarContrasenia";
			this.btnMostrarContrasenia.Size = new System.Drawing.Size(25, 26);
			this.btnMostrarContrasenia.TabIndex = 9;
			this.btnMostrarContrasenia.UseVisualStyleBackColor = false;
			this.btnMostrarContrasenia.Click += new System.EventHandler(this.btnMostrarContrasenia_Click);
			// 
			// frmDetalleUsuario
			// 
			this.AcceptButton = this.btnGuardar;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.btnCancelar;
			this.ClientSize = new System.Drawing.Size(284, 234);
			this.Controls.Add(this.btnMostrarContrasenia);
			this.Controls.Add(this.btnEliminar);
			this.Controls.Add(this.campoTipo);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.campoContrasenia);
			this.Controls.Add(this.lblContrasenia);
			this.Controls.Add(this.campoUsuario);
			this.Controls.Add(this.lblUsuario);
			this.Controls.Add(this.campoNombre);
			this.Controls.Add(this.lblNombre);
			this.Controls.Add(this.campoId);
			this.Controls.Add(this.lblId);
			this.Controls.Add(this.btnCancelar);
			this.Controls.Add(this.btnGuardar);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmDetalleUsuario";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Detalle de usuario";
			this.Load += new System.EventHandler(this.frmDetalleUsuario_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion
		private System.Windows.Forms.Button btnCancelar;
		private System.Windows.Forms.Label lblId;
		private System.Windows.Forms.Label lblNombre;
		private System.Windows.Forms.Label lblUsuario;
		private System.Windows.Forms.Label lblContrasenia;
		private System.Windows.Forms.Label label5;
		public System.Windows.Forms.Button btnGuardar;
		public System.Windows.Forms.TextBox campoId;
		public System.Windows.Forms.TextBox campoNombre;
		public System.Windows.Forms.TextBox campoUsuario;
		public System.Windows.Forms.TextBox campoContrasenia;
		public System.Windows.Forms.ComboBox campoTipo;
		public System.Windows.Forms.Button btnEliminar;
		public System.Windows.Forms.Button btnMostrarContrasenia;
	}
}