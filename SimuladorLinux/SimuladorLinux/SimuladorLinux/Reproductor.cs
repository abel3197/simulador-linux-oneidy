﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WMPLib;

namespace SimuladorLinux
{
	public partial class frmReproductor : Form
	{
		IWMPMedia cancionQueSeRepite;
		IWMPPlaylist listaReproduccion;

		public frmReproductor()
		{
			InitializeComponent();
			frmContenedor.ConfigurarMdiParent(this);
		}

		private void Reproductor_Load(object sender, EventArgs e)
		{
			listaReproduccion = wmpReproductor.currentPlaylist;
			listaReproduccion.name = "Lista de reproducción de " + Usuario.actual.nombre;
			//listaReproduccion.clear();
			wmpReproductor.settings.setMode("showFrame", true);
			wmpReproductor.settings.setMode("autoRewind", false);
		}

		private void btnOcultarLista_Click(object sender, EventArgs e)
		{
			if (btnOcultarLista.Text == "◀️")
			{
				btnOcultarLista.Text = "▶️";
				splDivisor.Panel2Collapsed = false;
			}
			else
			{
				btnOcultarLista.Text = "◀️";
				splDivisor.Panel2Collapsed = true;
			}
		}

		private void lvwLista_MouseDoubleClick(object sender, MouseEventArgs e)
		{
			if (lvwLista.SelectedItems.Count == 1)
			{
				ListViewItem item = lvwLista.SelectedItems[0];
				cancionQueSeRepite = (IWMPMedia)item.Tag;
				wmpReproductor.Ctlcontrols.playItem(cancionQueSeRepite);
			}
		}

		private void btnLimpiar_Click(object sender, EventArgs e)
		{
			lvwLista.Items.Clear();
			listaReproduccion.clear();
		}

		private void btnPantallaCompletas_Click(object sender, EventArgs e)
		{
			if (wmpReproductor.playState == WMPPlayState.wmppsPlaying)
			{
				//Console.WriteLine("Estado: {0}", wmpReproductor.playState);
				wmpReproductor.fullScreen = true;
			}
		}

		private void btnAgregar_Click(object sender, EventArgs e)
		{
			OpenFileDialog fileDialog = new OpenFileDialog();
			fileDialog.InitialDirectory = frmExplorador.RutaUsuario();
			string audios = "*.mp3;*.mid;*.midi;*.wav;*.acc;*.ac3;*.flac";
			string imagenes = "*.png;*.bpm;*.jpg;*.jpeg;*.ico;*.gif;*.tif;*.tiff";
			string videos = "*.mp4;*.mpg;*.mov;*.wmv;*.avi;*.dvd;*.mpe;*.mpeg";
			fileDialog.Filter = "Audios|" + audios +
				"|Imágenes|" + imagenes +
				"|Videos|" + videos +
				"|Audios, imágenes y videos|" + audios + ";" + imagenes + ";" + videos +
				"|Todos los archivos|*.*";
			fileDialog.CheckFileExists = true;
			fileDialog.Multiselect = true;
			fileDialog.Title = "Agregar archivos a lista de reproducción";
			DialogResult opcion = fileDialog.ShowDialog(this);
			if (opcion == DialogResult.OK)
			{
				bool listaEstuvoVacia = lvwLista.Items.Count == 0;
				string nombreFallidos = "";
				for (int i = 0; i < fileDialog.FileNames.Length; i++)
				{
					string nombreArchivo = fileDialog.SafeFileNames[i];
					string rutaArchivo = fileDialog.FileNames[i];
					try
					{
						IWMPMedia media = wmpReproductor.newMedia(rutaArchivo);

						ListViewItem item = new ListViewItem(nombreArchivo);
						item.SubItems.AddRange(new string[] { media.name, rutaArchivo });
						lvwLista.Items.Add(item);

						item.Tag = (IWMPMedia)media;
						listaReproduccion.appendItem(media);
					}
					catch (Exception ex)
					{
						nombreFallidos += "\n- " + nombreArchivo + ": " + ex.Message.Substring(0, 30);
					}
				}
				if (nombreFallidos.Length > 0)
				{
					MessageBox.Show(
						"No hemos podido agregar estos archivos a la lista:" + nombreFallidos,
						"Error abriendo archivo",
						MessageBoxButtons.OK,
						MessageBoxIcon.Error
					);
				}
				bool listaTieneElementos = lvwLista.Items.Count > 0;

				if (listaEstuvoVacia && listaTieneElementos)
				{
					wmpReproductor.Ctlcontrols.play();
				}
			}
		}

		private void btnEliminar_Click(object sender, EventArgs e)
		{
			foreach (ListViewItem item in lvwLista.SelectedItems)
			{
				listaReproduccion.removeItem((IWMPMedia)item.Tag);
				lvwLista.Items.Remove(item);
			}
		}

		private void wmpReproductor_CurrentItemChange(object sender, AxWMPLib._WMPOCXEvents_CurrentItemChangeEvent e)
		{
			//Console.WriteLine("wmpReproductor_CurrentItemChange: {0}", ((IWMPMedia)e.pdispMedia).name);
		}
		private void wmpReproductor_MediaChange(object sender, AxWMPLib._WMPOCXEvents_MediaChangeEvent e)
		{
			//Console.WriteLine("wmpReproductor_MediaChange: {0}", ((IWMPMedia)e.item).name);
		}
		private void wmpReproductor_ModeChange(object sender, AxWMPLib._WMPOCXEvents_ModeChangeEvent e)
		{
			//Console.WriteLine("wmpReproductor_ModeChange: {0} - {1}", e.modeName, e.newValue);
		}

		private void wmpReproductor_PlayStateChange(object sender, AxWMPLib._WMPOCXEvents_PlayStateChangeEvent e)
		{
			ActualizarColorEnLista(e.newState);
		}
		string ObtenerIdDeMedia(IWMPMedia media)
		{
			string TrackingID = media == null ? null : media.getItemInfo("TrackingID");
			return TrackingID;
		}
		void ActualizarColorEnLista(int playState)
		{
			bool noEstaReproduciendo = playState != (int)WMPPlayState.wmppsPlaying;
			foreach (ListViewItem item in lvwLista.Items)
			{
				string mediaReproduciendo = ObtenerIdDeMedia(wmpReproductor.currentMedia);
				string mediaItem = ObtenerIdDeMedia((IWMPMedia)item.Tag);
				bool itemNoReproduciendo = mediaReproduciendo != mediaItem;
				if (noEstaReproduciendo || itemNoReproduciendo)
				{
					item.ForeColor = Color.Empty;
				}
				else
				{
					item.ForeColor = Color.Blue;
				}
			}
		}

		private void btnModoReproduccion_Click(object sender, EventArgs e)
		{
			if (btnModoReproduccion.Text == "🔀" && btnModoReproduccion.ForeColor == Color.Gray)
			{
				wmpReproductor.settings.setMode("shuffle", true);
				btnModoReproduccion.ForeColor = Color.Blue;
			}
			else if (btnModoReproduccion.Text == "🔀")
			{
				wmpReproductor.settings.setMode("shuffle", false);
				wmpReproductor.settings.setMode("loop", true);
				btnModoReproduccion.Text = "🔁";
			}
			else if (btnModoReproduccion.Text == "🔁")
			{
				wmpReproductor.settings.setMode("loop", false);
				wmpReproductor.PlayStateChange += RepetirUnaCancion;
				//wmpReproductor.settings.playCount = 100;
				btnModoReproduccion.Text = "🔂";
			}
			else
			{
				wmpReproductor.PlayStateChange -= RepetirUnaCancion;
				//wmpReproductor.settings.playCount = 1;
				btnModoReproduccion.Text = "🔀";
				btnModoReproduccion.ForeColor = Color.Gray;
			}
		}

		private void RepetirUnaCancion(object sender, AxWMPLib._WMPOCXEvents_PlayStateChangeEvent e)
		{
			// Probar quitando la lista del control, agregandola y reproduciendo archivo
			// Probar dejando la playlist autoRewind y loop en true y verificar si cambia de la media primera a la media ultima. Si está en modo repetir una debe reproducir la última.
			// Probar si asignando la URL del elemento a reproducir no se cre una lista nueva si ya la ruta se encuentra en la lista actual
			bool terminoUnaReproduccion = e.newState == (int)WMPPlayState.wmppsMediaEnded;
			if (terminoUnaReproduccion)
			{
				cancionQueSeRepite = wmpReproductor.currentMedia;
			}
			Console.WriteLine("wmpReproductor_PlayStateChange: {0}\n" +
				"currentMedia: {1}\n" +
				"cancionQueSeRepite: {2}\n" +
				"segundos: {3}\n" +
				"\n",
				Enum.GetName(WMPPlayState.wmppsUndefined.GetType(), e.newState),
				 wmpReproductor.currentMedia == null ? null : wmpReproductor.currentMedia.name,
				cancionQueSeRepite == null ? null : cancionQueSeRepite.name,
				wmpReproductor.Ctlcontrols.currentPositionString
			);
			if (cancionQueSeRepite != null)
			{
				bool cambiandoReproduccion = e.newState == (int)WMPPlayState.wmppsTransitioning;
				bool terminoReproducciones = e.newState == (int)WMPPlayState.wmppsStopped;
				string textoIndiceMediaActualEnLista = wmpReproductor.currentMedia == null ? null : wmpReproductor.currentMedia.getItemInfo("PlaylistIndex");
				if (cambiandoReproduccion || terminoReproducciones)
				{
					wmpReproductor.Ctlcontrols.playItem(cancionQueSeRepite);
					//wmpReproductor.Ctlcontrols.currentItem = cancionQueSeRepite;
					bool mediaActualNoEsLaUltima = (listaReproduccion.count - 1).ToString() != textoIndiceMediaActualEnLista;
					if (mediaActualNoEsLaUltima)
					{
						cancionQueSeRepite = null;
					}
					if (terminoReproducciones)
					{
						wmpReproductor.Ctlcontrols.play();
					}
				}
			}
			//if (terminoUnaReproduccion || terminoReproducciones)
			//{
			//	string textoIndiceMediaActualEnLista = wmpReproductor.currentMedia.getItemInfo("PlaylistIndex");
			//	int numeroIndice = Int32.Parse(textoIndiceMediaActualEnLista);
			//	//wmpReproductor.Ctlcontrols.playItem(listaReproduccion.Item[numeroIndice]);
			//	wmpReproductor.Ctlcontrols.currentItem = listaReproduccion.Item[numeroIndice];
			//	wmpReproductor.Ctlcontrols.play();
			//}
		}

		private void wmpReproductor_StatusChange(object sender, EventArgs e)
		{
			//Console.WriteLine("wmpReproductor_StatusChange: {0}", wmpReproductor.status);
			//if (wmpReproductor.status == "Finalizado")
			//{
			//	//wmpReproductor.Ctlcontrols.currentPosition = 1;
			//	//wmpReproductor.Ctlcontrols.playItem(wmpReproductor.currentMedia);
			//}
		}

		private void btnAyuda_Click(object sender, EventArgs e)
		{
			frmAyuda.AbrirAyuda("reproductor");
		}
	}
}
