﻿namespace SimuladorLinux
{
	partial class frmReproductor
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmReproductor));
			this.wmpReproductor = new AxWMPLib.AxWindowsMediaPlayer();
			this.splDivisor = new System.Windows.Forms.SplitContainer();
			this.btnModoReproduccion = new System.Windows.Forms.Button();
			this.btnPantallaCompletas = new System.Windows.Forms.Button();
			this.btnOcultarLista = new System.Windows.Forms.Button();
			this.btnEliminar = new System.Windows.Forms.Button();
			this.btnAgregar = new System.Windows.Forms.Button();
			this.pnlControlLista = new System.Windows.Forms.Panel();
			this.label1 = new System.Windows.Forms.Label();
			this.btnLimpiar = new System.Windows.Forms.Button();
			this.lvwLista = new System.Windows.Forms.ListView();
			this.colNombre = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.colTitulo = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.colRuta = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.btnAyuda = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.wmpReproductor)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.splDivisor)).BeginInit();
			this.splDivisor.Panel1.SuspendLayout();
			this.splDivisor.Panel2.SuspendLayout();
			this.splDivisor.SuspendLayout();
			this.pnlControlLista.SuspendLayout();
			this.SuspendLayout();
			// 
			// wmpReproductor
			// 
			this.wmpReproductor.Dock = System.Windows.Forms.DockStyle.Fill;
			this.wmpReproductor.Enabled = true;
			this.wmpReproductor.Location = new System.Drawing.Point(0, 0);
			this.wmpReproductor.MinimumSize = new System.Drawing.Size(212, 0);
			this.wmpReproductor.Name = "wmpReproductor";
			this.wmpReproductor.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("wmpReproductor.OcxState")));
			this.wmpReproductor.Size = new System.Drawing.Size(449, 401);
			this.wmpReproductor.TabIndex = 0;
			this.wmpReproductor.PlayStateChange += new AxWMPLib._WMPOCXEvents_PlayStateChangeEventHandler(this.wmpReproductor_PlayStateChange);
			this.wmpReproductor.StatusChange += new System.EventHandler(this.wmpReproductor_StatusChange);
			this.wmpReproductor.MediaChange += new AxWMPLib._WMPOCXEvents_MediaChangeEventHandler(this.wmpReproductor_MediaChange);
			this.wmpReproductor.CurrentItemChange += new AxWMPLib._WMPOCXEvents_CurrentItemChangeEventHandler(this.wmpReproductor_CurrentItemChange);
			this.wmpReproductor.ModeChange += new AxWMPLib._WMPOCXEvents_ModeChangeEventHandler(this.wmpReproductor_ModeChange);
			// 
			// splDivisor
			// 
			this.splDivisor.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.splDivisor.Location = new System.Drawing.Point(0, 0);
			this.splDivisor.Name = "splDivisor";
			// 
			// splDivisor.Panel1
			// 
			this.splDivisor.Panel1.Controls.Add(this.btnModoReproduccion);
			this.splDivisor.Panel1.Controls.Add(this.btnPantallaCompletas);
			this.splDivisor.Panel1.Controls.Add(this.btnOcultarLista);
			this.splDivisor.Panel1.Controls.Add(this.wmpReproductor);
			this.splDivisor.Panel1MinSize = 214;
			// 
			// splDivisor.Panel2
			// 
			this.splDivisor.Panel2.Controls.Add(this.btnAyuda);
			this.splDivisor.Panel2.Controls.Add(this.btnEliminar);
			this.splDivisor.Panel2.Controls.Add(this.btnAgregar);
			this.splDivisor.Panel2.Controls.Add(this.pnlControlLista);
			this.splDivisor.Panel2.Controls.Add(this.btnLimpiar);
			this.splDivisor.Panel2.Controls.Add(this.lvwLista);
			this.splDivisor.Panel2MinSize = 130;
			this.splDivisor.Size = new System.Drawing.Size(700, 401);
			this.splDivisor.SplitterDistance = 449;
			this.splDivisor.TabIndex = 1;
			// 
			// btnModoReproduccion
			// 
			this.btnModoReproduccion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnModoReproduccion.BackColor = System.Drawing.Color.Transparent;
			this.btnModoReproduccion.FlatAppearance.BorderSize = 0;
			this.btnModoReproduccion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnModoReproduccion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnModoReproduccion.ForeColor = System.Drawing.Color.Gray;
			this.btnModoReproduccion.Location = new System.Drawing.Point(211, 372);
			this.btnModoReproduccion.Name = "btnModoReproduccion";
			this.btnModoReproduccion.Size = new System.Drawing.Size(20, 20);
			this.btnModoReproduccion.TabIndex = 3;
			this.btnModoReproduccion.Text = "🔀";
			this.btnModoReproduccion.UseVisualStyleBackColor = false;
			this.btnModoReproduccion.Click += new System.EventHandler(this.btnModoReproduccion_Click);
			// 
			// btnPantallaCompletas
			// 
			this.btnPantallaCompletas.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnPantallaCompletas.BackColor = System.Drawing.Color.Transparent;
			this.btnPantallaCompletas.FlatAppearance.BorderSize = 0;
			this.btnPantallaCompletas.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnPantallaCompletas.ForeColor = System.Drawing.SystemColors.MenuHighlight;
			this.btnPantallaCompletas.Location = new System.Drawing.Point(406, 372);
			this.btnPantallaCompletas.Name = "btnPantallaCompletas";
			this.btnPantallaCompletas.Size = new System.Drawing.Size(20, 20);
			this.btnPantallaCompletas.TabIndex = 2;
			this.btnPantallaCompletas.Text = "📺";
			this.btnPantallaCompletas.UseVisualStyleBackColor = false;
			this.btnPantallaCompletas.Click += new System.EventHandler(this.btnPantallaCompletas_Click);
			// 
			// btnOcultarLista
			// 
			this.btnOcultarLista.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnOcultarLista.BackColor = System.Drawing.Color.Transparent;
			this.btnOcultarLista.FlatAppearance.BorderSize = 0;
			this.btnOcultarLista.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnOcultarLista.ForeColor = System.Drawing.SystemColors.MenuHighlight;
			this.btnOcultarLista.Location = new System.Drawing.Point(427, 372);
			this.btnOcultarLista.Name = "btnOcultarLista";
			this.btnOcultarLista.Size = new System.Drawing.Size(20, 20);
			this.btnOcultarLista.TabIndex = 1;
			this.btnOcultarLista.Text = "▶️";
			this.btnOcultarLista.UseVisualStyleBackColor = false;
			this.btnOcultarLista.Click += new System.EventHandler(this.btnOcultarLista_Click);
			// 
			// btnEliminar
			// 
			this.btnEliminar.Location = new System.Drawing.Point(32, 368);
			this.btnEliminar.Name = "btnEliminar";
			this.btnEliminar.Size = new System.Drawing.Size(33, 33);
			this.btnEliminar.TabIndex = 5;
			this.btnEliminar.Text = "x";
			this.btnEliminar.UseVisualStyleBackColor = true;
			this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
			// 
			// btnAgregar
			// 
			this.btnAgregar.Location = new System.Drawing.Point(0, 368);
			this.btnAgregar.Name = "btnAgregar";
			this.btnAgregar.Size = new System.Drawing.Size(33, 33);
			this.btnAgregar.TabIndex = 2;
			this.btnAgregar.Text = "+";
			this.btnAgregar.UseVisualStyleBackColor = true;
			this.btnAgregar.Click += new System.EventHandler(this.btnAgregar_Click);
			// 
			// pnlControlLista
			// 
			this.pnlControlLista.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.pnlControlLista.Controls.Add(this.label1);
			this.pnlControlLista.Location = new System.Drawing.Point(0, 0);
			this.pnlControlLista.Name = "pnlControlLista";
			this.pnlControlLista.Size = new System.Drawing.Size(208, 33);
			this.pnlControlLista.TabIndex = 4;
			// 
			// label1
			// 
			this.label1.AutoEllipsis = true;
			this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label1.Location = new System.Drawing.Point(0, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(208, 33);
			this.label1.TabIndex = 4;
			this.label1.Text = "Lista de reproducción";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// btnLimpiar
			// 
			this.btnLimpiar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnLimpiar.Location = new System.Drawing.Point(214, 368);
			this.btnLimpiar.Name = "btnLimpiar";
			this.btnLimpiar.Size = new System.Drawing.Size(33, 33);
			this.btnLimpiar.TabIndex = 3;
			this.btnLimpiar.Text = "🔄";
			this.btnLimpiar.UseVisualStyleBackColor = true;
			this.btnLimpiar.Click += new System.EventHandler(this.btnLimpiar_Click);
			// 
			// lvwLista
			// 
			this.lvwLista.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.lvwLista.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.lvwLista.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colNombre,
            this.colTitulo,
            this.colRuta});
			this.lvwLista.FullRowSelect = true;
			this.lvwLista.HideSelection = false;
			this.lvwLista.Location = new System.Drawing.Point(0, 36);
			this.lvwLista.Name = "lvwLista";
			this.lvwLista.Size = new System.Drawing.Size(246, 332);
			this.lvwLista.TabIndex = 0;
			this.lvwLista.UseCompatibleStateImageBehavior = false;
			this.lvwLista.View = System.Windows.Forms.View.Details;
			this.lvwLista.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lvwLista_MouseDoubleClick);
			// 
			// colNombre
			// 
			this.colNombre.Text = "Nombre";
			this.colNombre.Width = 200;
			// 
			// colTitulo
			// 
			this.colTitulo.Text = "Título";
			this.colTitulo.Width = 200;
			// 
			// colRuta
			// 
			this.colRuta.Text = "Ruta";
			this.colRuta.Width = 300;
			// 
			// btnAyuda
			// 
			this.btnAyuda.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnAyuda.BackColor = System.Drawing.Color.Transparent;
			this.btnAyuda.BackgroundImage = global::SimuladorLinux.Properties.Resources.help_person;
			this.btnAyuda.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.btnAyuda.FlatAppearance.BorderSize = 0;
			this.btnAyuda.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnAyuda.Location = new System.Drawing.Point(214, 3);
			this.btnAyuda.Name = "btnAyuda";
			this.btnAyuda.Size = new System.Drawing.Size(30, 30);
			this.btnAyuda.TabIndex = 42;
			this.btnAyuda.UseVisualStyleBackColor = false;
			this.btnAyuda.Click += new System.EventHandler(this.btnAyuda_Click);
			// 
			// frmReproductor
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(700, 401);
			this.Controls.Add(this.splDivisor);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MinimumSize = new System.Drawing.Size(380, 220);
			this.Name = "frmReproductor";
			this.ShowInTaskbar = false;
			this.Text = "Reproductor";
			this.Load += new System.EventHandler(this.Reproductor_Load);
			((System.ComponentModel.ISupportInitialize)(this.wmpReproductor)).EndInit();
			this.splDivisor.Panel1.ResumeLayout(false);
			this.splDivisor.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.splDivisor)).EndInit();
			this.splDivisor.ResumeLayout(false);
			this.pnlControlLista.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private AxWMPLib.AxWindowsMediaPlayer wmpReproductor;
		private System.Windows.Forms.SplitContainer splDivisor;
		private System.Windows.Forms.Button btnAgregar;
		private System.Windows.Forms.Button btnOcultarLista;
		private System.Windows.Forms.ListView lvwLista;
		private System.Windows.Forms.Button btnLimpiar;
		private System.Windows.Forms.Panel pnlControlLista;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ColumnHeader colNombre;
		private System.Windows.Forms.ColumnHeader colTitulo;
		private System.Windows.Forms.ColumnHeader colRuta;
		private System.Windows.Forms.Button btnPantallaCompletas;
		private System.Windows.Forms.Button btnEliminar;
		private System.Windows.Forms.Button btnModoReproduccion;
		private System.Windows.Forms.Button btnAyuda;
	}
}