﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SimuladorLinux
{
	public partial class frmContenedor : Form
	{
		static public frmContenedor contenedor;
		static frmEquipo equipo;
		private frmLogin login;

		public frmContenedor()
		{
			InitializeComponent();
			contenedor = this;
			MyNativeMDIclient nw = new MyNativeMDIclient(this.Controls.OfType<MdiClient>().FirstOrDefault());
			this.AutoScroll = false;
			login = new frmLogin();
			equipo = new frmEquipo();
			equipo.FormClosing += (s, e) =>
			{
				login.Show();
				pnlSistema.Visible = true;
			};
		}

		private void ContenedorMDI_Load(object sender, EventArgs e)
		{
			equipo.Show();
			frmExplorador.VerificarCrearCarpeta(frmExplorador.RutaBase());
		}

		static public void ConfigurarMdiParent(Form form)
		{
			form.MdiParent = contenedor;
			if (contenedor != null)
			{
				form.Activated += (s, e) =>
				{
					contenedor.lblTituloVentana.Text = form.Text;
				};
			}
		}

		private void tmrActualizadorHora_Tick(object sender, EventArgs e)
		{
			lblFechaHora.Text = DateTime.Now.ToString("dddd, d 'de' MMMM 'del' yyyy, hh:mm:ss tt");
		}

		private void btnApagar_Click(object sender, EventArgs e)
		{
			DialogResult opcion = MessageBox.Show("¿Seguro que desea apagar el equipo?", "Apagar equipo", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
			if (opcion == DialogResult.OK)
			{
				equipo = new frmEquipo("apagando");
				pnlSistema.Visible = false;
				equipo.Show();
			}
		}
	}

	// C# MDI - how do I prevent the scroll bar? https://stackoverflow.com/a/7944450/13415998
	internal class MyNativeMDIclient : NativeWindow
	{
		private MdiClient mdiClient;

		public MyNativeMDIclient(MdiClient parent)
		{
			mdiClient = parent;
			ReleaseHandle();
			AssignHandle(mdiClient.Handle);
		}
		internal void OnHandleDestroyed(object sender, EventArgs e)
		{
			ReleaseHandle();
		}
		private const int SB_BOTH = 3;
		[System.Runtime.InteropServices.DllImport("user32.dll")]
		private static extern int ShowScrollBar(IntPtr hWnd, int wBar, int bShow);
		protected override void WndProc(ref Message m)
		{
			ShowScrollBar(m.HWnd, SB_BOTH, 0 /*false*/);
			base.WndProc(ref m);
		}
	}
}
