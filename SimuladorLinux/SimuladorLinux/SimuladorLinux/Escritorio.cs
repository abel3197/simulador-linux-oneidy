﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SimuladorLinux
{
	public partial class frmEscritorio : Form
	{
		static public frmEscritorio escritorio;

		public frmEscritorio()
		{
			InitializeComponent();
			escritorio = this;
			frmContenedor.ConfigurarMdiParent(this);
			if (this.MdiParent != null)
			{
				MdiClient mdiClient = this.MdiParent.Controls.OfType<MdiClient>().FirstOrDefault();
				Size tamañoMDI = new Size(mdiClient.Size.Width /*- 5*/, mdiClient.Size.Height /*- 5*/);
				this.Size = tamañoMDI;
			}
			imgBlocNotas.MouseEnter += IconoBarraTarea_MouseEnter;
			imgBlocNotas.MouseLeave += IconoBarraTarea_MouseLeave;
			imgBlocNotas.Click += imgBlocNotas_Click;
			imgCalculadora.MouseEnter += IconoBarraTarea_MouseEnter;
			imgCalculadora.MouseLeave += IconoBarraTarea_MouseLeave;
			imgCalculadora.Click += imgCalculadora_Click;
			imgReproductor.MouseEnter += IconoBarraTarea_MouseEnter;
			imgReproductor.MouseLeave += IconoBarraTarea_MouseLeave;
			imgReproductor.Click += imgReproductor_Click;
			imgUsuarios.MouseEnter += IconoBarraTarea_MouseEnter;
			imgUsuarios.MouseLeave += IconoBarraTarea_MouseLeave;
			imgUsuarios.Click += imgUsuarios_Click;
			imgExplorador.MouseEnter += IconoBarraTarea_MouseEnter;
			imgExplorador.MouseLeave += IconoBarraTarea_MouseLeave;
			imgExplorador.Click += imgExplorador_Click;
			this.fswEscritorio.Path = frmExplorador.RutaUsuario();
			this.fswEscritorio.EnableRaisingEvents = true;
		}

		private void Escritorio_Load(object sender, EventArgs e)
		{
			try
			{
				Image imagen = Image.FromFile(frmExplorador.RutaUsuario() + "fondo");
				CambiarImagenFondo(imagen);
			}
			catch { }
			CheckForIllegalCrossThreadCalls = false;
			new Thread(CargarIconosEscritorio).Start();
		}

		bool actualizandoIconos;
		private void CargarIconosEscritorio()
		{
			if (this.InvokeRequired)
			{
				this.Invoke(new MethodInvoker(() => CargarIconosEscritorio()));
			}
			else
			{
				if (actualizandoIconos) return;
				actualizandoIconos = true;
				Console.WriteLine("Actualizando escritorio " + new DateTime());
				this.flpIconos.Controls.Clear();
				DirectoryInfo carpetaEscritorio = new DirectoryInfo(frmExplorador.RutaInicio());
				DirectoryInfo[] carpetasEscritorio = carpetaEscritorio.GetDirectories();
				foreach (var carpeta in carpetasEscritorio)
				{
					IconoEscritorio icono = new IconoEscritorio(carpeta);
					this.flpIconos.Controls.Add(icono.Control);
				}
				FileInfo[] archivosEscritorio = carpetaEscritorio.GetFiles();
				foreach (var archivo in archivosEscritorio)
				{
					IconoEscritorio icono = new IconoEscritorio(archivo);
					this.flpIconos.Controls.Add(icono.Control);
				}
				actualizandoIconos = false;
			}
		}

		public void CambiarImagenFondo(Image imagen)
		{
			this.BackgroundImage = imagen;
			if (frmContenedor.contenedor != null)
			{
				frmContenedor.contenedor.BackgroundImage = imagen;
			}
		}

		private void lstIconos_SelectedIndexChanged(object sender, EventArgs e)
		{

		}

		private void frmEscritorio_Activated(object sender, EventArgs e)
		{
			this.SendToBack();
		}

		private void frmEscritorio_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (e.CloseReason == CloseReason.UserClosing)
				e.Cancel = true;
		}

		private void fswEscritorio_Changed(object sender, FileSystemEventArgs e)
		{
			new Thread(CargarIconosEscritorio).Start();
		}

		private void IconoBarraTarea_MouseEnter(object sender, EventArgs e)
		{
			Panel panel = (Panel)((PictureBox)sender).Parent;
			panel.BackgroundImage = Properties.Resources.marco_transparente7;
			//panel.BackgroundImage = Properties.Resources.marco_dorado;
		}

		private void IconoBarraTarea_MouseLeave(object sender, EventArgs e)
		{
			Panel panel = (Panel)((PictureBox)sender).Parent;
			if (panel.Tag != null)
			{
				panel.BackgroundImage = Properties.Resources.marco_transparente7;
			}
			else
			{
				panel.BackgroundImage = null;
			}
		}

		static public frmExplorador explorador;
		static public FormWindowState exploradorWindowState;
		private void imgExplorador_Click(object sender, EventArgs e)
		{
			if (explorador == null)
			{
				explorador = new frmExplorador();
				exploradorWindowState = explorador.WindowState;
				pnlExplorador.Tag = explorador;
				explorador.FormClosed += new FormClosedEventHandler(delegate (object btn, FormClosedEventArgs clickEvent)
				{
					pnlExplorador.Tag = explorador = null;
					pnlExplorador.BackgroundImage = null;
				});
				explorador.Show();
			}
			else
			{
				if (explorador.WindowState != FormWindowState.Minimized)
				{
					exploradorWindowState = explorador.WindowState;
					explorador.WindowState = FormWindowState.Minimized;
				}
				else
				{
					explorador.WindowState = exploradorWindowState;
				}
			}
		}

		static public frmCalculadora calc;
		static public FormWindowState calcWindowState;
		private void imgCalculadora_Click(object sender, EventArgs e)
		{
			if (calc == null)
			{
				calc = new frmCalculadora();
				calcWindowState = calc.WindowState;
				pnlCalculadora.Tag = calc;
				calc.FormClosed += new FormClosedEventHandler(delegate (object btn, FormClosedEventArgs clickEvent)
				{
					pnlCalculadora.Tag = calc = null;
					pnlCalculadora.BackgroundImage = null;
				});
				calc.Show();
			}
			else
			{
				if (calc.WindowState != FormWindowState.Minimized)
				{
					calcWindowState = calc.WindowState;
					calc.WindowState = FormWindowState.Minimized;
				}
				else
				{
					calc.WindowState = calcWindowState;
				}
			}
		}

		static public frmPanelUsuarios registro;
		static public FormWindowState registroWindowState;
		private void imgUsuarios_Click(object sender, EventArgs e)
		{
			if (registro == null)
			{
				registro = new frmPanelUsuarios();
				registroWindowState = registro.WindowState;
				pnlUsuarios.Tag = registro;
				registro.FormClosed += new FormClosedEventHandler(delegate (object btn, FormClosedEventArgs clickEvent)
				{
					pnlUsuarios.Tag = registro = null;
					pnlUsuarios.BackgroundImage = null;
				});
				registro.Show();
			}
			else
			{
				if (registro.WindowState != FormWindowState.Minimized)
				{
					registroWindowState = registro.WindowState;
					registro.WindowState = FormWindowState.Minimized;
				}
				else
				{
					registro.WindowState = registroWindowState;
				}
			}
		}

		static public frmBlocNotas blocNotas;
		static public FormWindowState blocNotasWindowState;
		private void imgBlocNotas_Click(object sender, EventArgs e)
		{
			if (blocNotas == null)
			{
				blocNotas = new frmBlocNotas();
				blocNotasWindowState = blocNotas.WindowState;
				pnlBlocNotas.Tag = blocNotas;
				blocNotas.FormClosed += new FormClosedEventHandler(delegate (object btn, FormClosedEventArgs clickEvent)
				{
					pnlBlocNotas.Tag = blocNotas = null;
					pnlBlocNotas.BackgroundImage = null;
				});
				blocNotas.Show();
			}
			else
			{
				if (blocNotas.WindowState != FormWindowState.Minimized)
				{
					blocNotasWindowState = blocNotas.WindowState;
					blocNotas.WindowState = FormWindowState.Minimized;
				}
				else
				{
					blocNotas.WindowState = blocNotasWindowState;
				}
			}
		}

		static public frmReproductor reproductor;
		static public FormWindowState reproductorWindowState;
		private void imgReproductor_Click(object sender, EventArgs e)
		{
			if (reproductor == null)
			{
				reproductor = new frmReproductor();
				reproductorWindowState = reproductor.WindowState;
				pnlReproductor.Tag = reproductor;
				reproductor.FormClosed += new FormClosedEventHandler(delegate (object btn, FormClosedEventArgs clickEvent)
				{
					pnlReproductor.Tag = reproductor = null;
					pnlReproductor.BackgroundImage = null;
				});
				reproductor.ResizeEnd += (res, ree) =>
				{
					if (reproductor.WindowState == FormWindowState.Minimized)
					{
						reproductor.Hide();
					}
				};
				reproductor.Show();
			}
			else
			{
				if (reproductor.WindowState != FormWindowState.Minimized)
				{
					reproductorWindowState = reproductor.WindowState;
					reproductor.WindowState = FormWindowState.Minimized;
					reproductor.Hide();
				}
				else
				{
					reproductor.WindowState = reproductorWindowState;
					reproductor.Show();
				}
			}
		}

		void AlternarVisibilidad(Form form)
		{
			if (form.WindowState != FormWindowState.Minimized)
			{
				reproductorWindowState = form.WindowState;
				form.WindowState = FormWindowState.Minimized;
				form.Hide();
			}
			else
			{
				form.WindowState = reproductorWindowState;
			}
		}
	}

}
