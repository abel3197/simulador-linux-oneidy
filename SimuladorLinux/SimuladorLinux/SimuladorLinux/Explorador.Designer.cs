﻿namespace SimuladorLinux
{
	partial class frmExplorador
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmExplorador));
			this.wbrFolder = new System.Windows.Forms.WebBrowser();
			this.btnAtras = new System.Windows.Forms.Button();
			this.btnAlante = new System.Windows.Forms.Button();
			this.cbRutas = new System.Windows.Forms.ComboBox();
			this.txtRuta = new System.Windows.Forms.TextBox();
			this.btnNavegar = new System.Windows.Forms.Button();
			this.btnRefrescar = new System.Windows.Forms.Button();
			this.btnInicio = new System.Windows.Forms.Button();
			this.btnAyuda = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// wbrFolder
			// 
			this.wbrFolder.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.wbrFolder.Location = new System.Drawing.Point(0, 33);
			this.wbrFolder.MinimumSize = new System.Drawing.Size(20, 20);
			this.wbrFolder.Name = "wbrFolder";
			this.wbrFolder.Size = new System.Drawing.Size(733, 426);
			this.wbrFolder.TabIndex = 0;
			this.wbrFolder.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.wbrFolder_DocumentCompleted);
			// 
			// btnAtras
			// 
			this.btnAtras.Location = new System.Drawing.Point(-1, -1);
			this.btnAtras.Name = "btnAtras";
			this.btnAtras.Size = new System.Drawing.Size(30, 30);
			this.btnAtras.TabIndex = 1;
			this.btnAtras.Text = "<";
			this.btnAtras.UseVisualStyleBackColor = true;
			this.btnAtras.Click += new System.EventHandler(this.btnAtras_Click);
			// 
			// btnAlante
			// 
			this.btnAlante.Location = new System.Drawing.Point(28, -1);
			this.btnAlante.Name = "btnAlante";
			this.btnAlante.Size = new System.Drawing.Size(30, 30);
			this.btnAlante.TabIndex = 2;
			this.btnAlante.Text = ">";
			this.btnAlante.UseVisualStyleBackColor = true;
			this.btnAlante.Click += new System.EventHandler(this.btnAlante_Click);
			// 
			// cbRutas
			// 
			this.cbRutas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbRutas.DropDownWidth = 200;
			this.cbRutas.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cbRutas.FormattingEnabled = true;
			this.cbRutas.ItemHeight = 20;
			this.cbRutas.Location = new System.Drawing.Point(87, 0);
			this.cbRutas.Name = "cbRutas";
			this.cbRutas.Size = new System.Drawing.Size(18, 28);
			this.cbRutas.TabIndex = 3;
			this.cbRutas.SelectedIndexChanged += new System.EventHandler(this.cbRutas_SelectedIndexChanged);
			// 
			// txtRuta
			// 
			this.txtRuta.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.txtRuta.Font = new System.Drawing.Font("Times New Roman", 13.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtRuta.Location = new System.Drawing.Point(104, -1);
			this.txtRuta.Name = "txtRuta";
			this.txtRuta.Size = new System.Drawing.Size(544, 29);
			this.txtRuta.TabIndex = 4;
			// 
			// btnNavegar
			// 
			this.btnNavegar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnNavegar.Location = new System.Drawing.Point(646, -1);
			this.btnNavegar.Name = "btnNavegar";
			this.btnNavegar.Size = new System.Drawing.Size(30, 30);
			this.btnNavegar.TabIndex = 5;
			this.btnNavegar.Text = "→";
			this.btnNavegar.UseVisualStyleBackColor = true;
			this.btnNavegar.Click += new System.EventHandler(this.btnNavegar_Click);
			// 
			// btnRefrescar
			// 
			this.btnRefrescar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnRefrescar.Location = new System.Drawing.Point(675, -1);
			this.btnRefrescar.Name = "btnRefrescar";
			this.btnRefrescar.Size = new System.Drawing.Size(30, 30);
			this.btnRefrescar.TabIndex = 6;
			this.btnRefrescar.Text = "🔄";
			this.btnRefrescar.UseVisualStyleBackColor = true;
			this.btnRefrescar.Click += new System.EventHandler(this.btnRefrescar_Click);
			// 
			// btnInicio
			// 
			this.btnInicio.Location = new System.Drawing.Point(57, -1);
			this.btnInicio.Name = "btnInicio";
			this.btnInicio.Size = new System.Drawing.Size(30, 30);
			this.btnInicio.TabIndex = 7;
			this.btnInicio.Text = "🏠";
			this.btnInicio.UseVisualStyleBackColor = true;
			this.btnInicio.Click += new System.EventHandler(this.btnInicio_Click);
			// 
			// btnAyuda
			// 
			this.btnAyuda.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnAyuda.BackgroundImage = global::SimuladorLinux.Properties.Resources.help_person;
			this.btnAyuda.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.btnAyuda.Location = new System.Drawing.Point(704, -1);
			this.btnAyuda.Name = "btnAyuda";
			this.btnAyuda.Size = new System.Drawing.Size(30, 30);
			this.btnAyuda.TabIndex = 8;
			this.btnAyuda.UseVisualStyleBackColor = true;
			this.btnAyuda.Click += new System.EventHandler(this.btnAyuda_Click);
			// 
			// frmExplorador
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(733, 457);
			this.Controls.Add(this.btnNavegar);
			this.Controls.Add(this.btnAyuda);
			this.Controls.Add(this.btnInicio);
			this.Controls.Add(this.btnRefrescar);
			this.Controls.Add(this.cbRutas);
			this.Controls.Add(this.btnAlante);
			this.Controls.Add(this.btnAtras);
			this.Controls.Add(this.wbrFolder);
			this.Controls.Add(this.txtRuta);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "frmExplorador";
			this.Text = "Explorador de archivos";
			this.Load += new System.EventHandler(this.Explorador_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.WebBrowser wbrFolder;
		private System.Windows.Forms.Button btnAtras;
		private System.Windows.Forms.Button btnAlante;
		private System.Windows.Forms.ComboBox cbRutas;
		public System.Windows.Forms.TextBox txtRuta;
		public System.Windows.Forms.Button btnNavegar;
		private System.Windows.Forms.Button btnRefrescar;
		private System.Windows.Forms.Button btnInicio;
		private System.Windows.Forms.Button btnAyuda;
	}
}