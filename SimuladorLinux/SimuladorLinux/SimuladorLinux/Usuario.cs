﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimuladorLinux
{
	public class Usuario
	{
		static public Usuario actual = new Usuario("3", "Oneidy Tapia", "one", "1234", "Administrador");

		public string id;
		public string nombre;
		public string usuario;
		public string contrasenia;
		public string tipo;

		public Usuario()
		{
		}

		public Usuario(string id, string nombre, string usuario, string contrasenia, string tipo)
		{
			this.id = id;
			this.nombre = nombre;
			this.usuario = usuario;
			this.contrasenia = contrasenia;
			this.tipo = tipo;
		}
	}
}
