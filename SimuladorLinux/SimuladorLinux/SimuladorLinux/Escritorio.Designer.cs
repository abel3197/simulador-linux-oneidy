﻿namespace SimuladorLinux
{
	partial class frmEscritorio
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEscritorio));
			this.flpIconos = new System.Windows.Forms.FlowLayoutPanel();
			this.pnlBarraTareas = new System.Windows.Forms.Panel();
			this.pnlExplorador = new System.Windows.Forms.Panel();
			this.imgExplorador = new System.Windows.Forms.PictureBox();
			this.pnlReproductor = new System.Windows.Forms.Panel();
			this.imgReproductor = new System.Windows.Forms.PictureBox();
			this.pnlUsuarios = new System.Windows.Forms.Panel();
			this.imgUsuarios = new System.Windows.Forms.PictureBox();
			this.pnlBlocNotas = new System.Windows.Forms.Panel();
			this.imgBlocNotas = new System.Windows.Forms.PictureBox();
			this.pnlCalculadora = new System.Windows.Forms.Panel();
			this.imgCalculadora = new System.Windows.Forms.PictureBox();
			this.fswEscritorio = new System.IO.FileSystemWatcher();
			this.pnlBarraTareas.SuspendLayout();
			this.pnlExplorador.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.imgExplorador)).BeginInit();
			this.pnlReproductor.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.imgReproductor)).BeginInit();
			this.pnlUsuarios.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.imgUsuarios)).BeginInit();
			this.pnlBlocNotas.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.imgBlocNotas)).BeginInit();
			this.pnlCalculadora.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.imgCalculadora)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fswEscritorio)).BeginInit();
			this.SuspendLayout();
			// 
			// flpIconos
			// 
			this.flpIconos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.flpIconos.BackColor = System.Drawing.Color.Transparent;
			this.flpIconos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.flpIconos.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
			this.flpIconos.Location = new System.Drawing.Point(61, 0);
			this.flpIconos.Name = "flpIconos";
			this.flpIconos.Size = new System.Drawing.Size(678, 496);
			this.flpIconos.TabIndex = 0;
			// 
			// pnlBarraTareas
			// 
			this.pnlBarraTareas.BackColor = System.Drawing.Color.Transparent;
			this.pnlBarraTareas.Controls.Add(this.pnlExplorador);
			this.pnlBarraTareas.Controls.Add(this.pnlReproductor);
			this.pnlBarraTareas.Controls.Add(this.pnlUsuarios);
			this.pnlBarraTareas.Controls.Add(this.pnlBlocNotas);
			this.pnlBarraTareas.Controls.Add(this.pnlCalculadora);
			this.pnlBarraTareas.Dock = System.Windows.Forms.DockStyle.Left;
			this.pnlBarraTareas.Location = new System.Drawing.Point(0, 0);
			this.pnlBarraTareas.Name = "pnlBarraTareas";
			this.pnlBarraTareas.Size = new System.Drawing.Size(60, 496);
			this.pnlBarraTareas.TabIndex = 1;
			// 
			// pnlExplorador
			// 
			this.pnlExplorador.BackColor = System.Drawing.Color.Transparent;
			this.pnlExplorador.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.pnlExplorador.Controls.Add(this.imgExplorador);
			this.pnlExplorador.Location = new System.Drawing.Point(0, 264);
			this.pnlExplorador.Name = "pnlExplorador";
			this.pnlExplorador.Size = new System.Drawing.Size(60, 60);
			this.pnlExplorador.TabIndex = 10;
			// 
			// imgExplorador
			// 
			this.imgExplorador.Image = global::SimuladorLinux.Properties.Resources.folder;
			this.imgExplorador.Location = new System.Drawing.Point(0, 0);
			this.imgExplorador.Name = "imgExplorador";
			this.imgExplorador.Size = new System.Drawing.Size(60, 60);
			this.imgExplorador.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.imgExplorador.TabIndex = 0;
			this.imgExplorador.TabStop = false;
			// 
			// pnlReproductor
			// 
			this.pnlReproductor.BackColor = System.Drawing.Color.Transparent;
			this.pnlReproductor.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.pnlReproductor.Controls.Add(this.imgReproductor);
			this.pnlReproductor.Location = new System.Drawing.Point(0, 198);
			this.pnlReproductor.Name = "pnlReproductor";
			this.pnlReproductor.Size = new System.Drawing.Size(60, 60);
			this.pnlReproductor.TabIndex = 9;
			// 
			// imgReproductor
			// 
			this.imgReproductor.Image = global::SimuladorLinux.Properties.Resources.media_ico;
			this.imgReproductor.Location = new System.Drawing.Point(0, 0);
			this.imgReproductor.Name = "imgReproductor";
			this.imgReproductor.Size = new System.Drawing.Size(60, 60);
			this.imgReproductor.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.imgReproductor.TabIndex = 0;
			this.imgReproductor.TabStop = false;
			// 
			// pnlUsuarios
			// 
			this.pnlUsuarios.BackColor = System.Drawing.Color.Transparent;
			this.pnlUsuarios.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.pnlUsuarios.Controls.Add(this.imgUsuarios);
			this.pnlUsuarios.Location = new System.Drawing.Point(0, 132);
			this.pnlUsuarios.Name = "pnlUsuarios";
			this.pnlUsuarios.Size = new System.Drawing.Size(60, 60);
			this.pnlUsuarios.TabIndex = 8;
			// 
			// imgUsuarios
			// 
			this.imgUsuarios.Image = global::SimuladorLinux.Properties.Resources.usuarios_ico;
			this.imgUsuarios.Location = new System.Drawing.Point(0, 0);
			this.imgUsuarios.Name = "imgUsuarios";
			this.imgUsuarios.Size = new System.Drawing.Size(60, 60);
			this.imgUsuarios.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.imgUsuarios.TabIndex = 0;
			this.imgUsuarios.TabStop = false;
			// 
			// pnlBlocNotas
			// 
			this.pnlBlocNotas.BackColor = System.Drawing.Color.Transparent;
			this.pnlBlocNotas.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.pnlBlocNotas.Controls.Add(this.imgBlocNotas);
			this.pnlBlocNotas.Location = new System.Drawing.Point(0, 66);
			this.pnlBlocNotas.Name = "pnlBlocNotas";
			this.pnlBlocNotas.Size = new System.Drawing.Size(60, 60);
			this.pnlBlocNotas.TabIndex = 7;
			// 
			// imgBlocNotas
			// 
			this.imgBlocNotas.Image = global::SimuladorLinux.Properties.Resources.note_ico;
			this.imgBlocNotas.Location = new System.Drawing.Point(0, 0);
			this.imgBlocNotas.Name = "imgBlocNotas";
			this.imgBlocNotas.Size = new System.Drawing.Size(60, 60);
			this.imgBlocNotas.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.imgBlocNotas.TabIndex = 0;
			this.imgBlocNotas.TabStop = false;
			// 
			// pnlCalculadora
			// 
			this.pnlCalculadora.BackColor = System.Drawing.Color.Transparent;
			this.pnlCalculadora.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.pnlCalculadora.Controls.Add(this.imgCalculadora);
			this.pnlCalculadora.Location = new System.Drawing.Point(0, 0);
			this.pnlCalculadora.Name = "pnlCalculadora";
			this.pnlCalculadora.Size = new System.Drawing.Size(60, 60);
			this.pnlCalculadora.TabIndex = 5;
			// 
			// imgCalculadora
			// 
			this.imgCalculadora.Image = global::SimuladorLinux.Properties.Resources.calc_51;
			this.imgCalculadora.Location = new System.Drawing.Point(0, 0);
			this.imgCalculadora.Name = "imgCalculadora";
			this.imgCalculadora.Size = new System.Drawing.Size(60, 60);
			this.imgCalculadora.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.imgCalculadora.TabIndex = 0;
			this.imgCalculadora.TabStop = false;
			// 
			// fswEscritorio
			// 
			this.fswEscritorio.EnableRaisingEvents = true;
			this.fswEscritorio.NotifyFilter = System.IO.NotifyFilters.LastWrite;
			this.fswEscritorio.SynchronizingObject = this;
			this.fswEscritorio.Changed += new System.IO.FileSystemEventHandler(this.fswEscritorio_Changed);
			// 
			// frmEscritorio
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.Black;
			this.BackgroundImage = global::SimuladorLinux.Properties.Resources.escritorio;
			this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.ClientSize = new System.Drawing.Size(737, 496);
			this.Controls.Add(this.pnlBarraTareas);
			this.Controls.Add(this.flpIconos);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmEscritorio";
			this.ShowIcon = false;
			this.ShowInTaskbar = false;
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.Text = "Escritorio";
			this.Activated += new System.EventHandler(this.frmEscritorio_Activated);
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmEscritorio_FormClosing);
			this.Load += new System.EventHandler(this.Escritorio_Load);
			this.pnlBarraTareas.ResumeLayout(false);
			this.pnlExplorador.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.imgExplorador)).EndInit();
			this.pnlReproductor.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.imgReproductor)).EndInit();
			this.pnlUsuarios.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.imgUsuarios)).EndInit();
			this.pnlBlocNotas.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.imgBlocNotas)).EndInit();
			this.pnlCalculadora.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.imgCalculadora)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fswEscritorio)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.FlowLayoutPanel flpIconos;
		private System.Windows.Forms.Panel pnlBarraTareas;
		private System.Windows.Forms.Panel pnlCalculadora;
		private System.Windows.Forms.PictureBox imgCalculadora;
		private System.Windows.Forms.Panel pnlBlocNotas;
		private System.Windows.Forms.PictureBox imgBlocNotas;
		private System.Windows.Forms.Panel pnlUsuarios;
		private System.Windows.Forms.PictureBox imgUsuarios;
		private System.Windows.Forms.Panel pnlReproductor;
		private System.Windows.Forms.PictureBox imgReproductor;
		private System.IO.FileSystemWatcher fswEscritorio;
		private System.Windows.Forms.Panel pnlExplorador;
		private System.Windows.Forms.PictureBox imgExplorador;
	}
}