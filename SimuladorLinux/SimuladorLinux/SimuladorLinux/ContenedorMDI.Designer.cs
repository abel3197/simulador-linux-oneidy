﻿namespace SimuladorLinux
{
	partial class frmContenedor
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmContenedor));
			this.tmrActualizadorHora = new System.Windows.Forms.Timer(this.components);
			this.btnApagar = new System.Windows.Forms.Button();
			this.pnlSistema = new System.Windows.Forms.Panel();
			this.lblFechaHora = new System.Windows.Forms.Label();
			this.lblTituloVentana = new System.Windows.Forms.Label();
			this.pnlSistema.SuspendLayout();
			this.SuspendLayout();
			// 
			// tmrActualizadorHora
			// 
			this.tmrActualizadorHora.Enabled = true;
			this.tmrActualizadorHora.Interval = 1000;
			this.tmrActualizadorHora.Tick += new System.EventHandler(this.tmrActualizadorHora_Tick);
			// 
			// btnApagar
			// 
			this.btnApagar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnApagar.BackColor = System.Drawing.Color.Transparent;
			this.btnApagar.BackgroundImage = global::SimuladorLinux.Properties.Resources.apagar2;
			this.btnApagar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.btnApagar.FlatAppearance.BorderSize = 0;
			this.btnApagar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnApagar.Location = new System.Drawing.Point(764, 0);
			this.btnApagar.Name = "btnApagar";
			this.btnApagar.Size = new System.Drawing.Size(25, 25);
			this.btnApagar.TabIndex = 3;
			this.btnApagar.UseVisualStyleBackColor = false;
			this.btnApagar.Click += new System.EventHandler(this.btnApagar_Click);
			// 
			// pnlSistema
			// 
			this.pnlSistema.BackgroundImage = global::SimuladorLinux.Properties.Resources.marco_transparente2;
			this.pnlSistema.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.pnlSistema.Controls.Add(this.btnApagar);
			this.pnlSistema.Controls.Add(this.lblFechaHora);
			this.pnlSistema.Controls.Add(this.lblTituloVentana);
			this.pnlSistema.Dock = System.Windows.Forms.DockStyle.Top;
			this.pnlSistema.Location = new System.Drawing.Point(0, 0);
			this.pnlSistema.Name = "pnlSistema";
			this.pnlSistema.Size = new System.Drawing.Size(800, 25);
			this.pnlSistema.TabIndex = 1;
			this.pnlSistema.Visible = false;
			// 
			// lblFechaHora
			// 
			this.lblFechaHora.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.lblFechaHora.BackColor = System.Drawing.Color.Transparent;
			this.lblFechaHora.ForeColor = System.Drawing.Color.White;
			this.lblFechaHora.Location = new System.Drawing.Point(531, 0);
			this.lblFechaHora.Name = "lblFechaHora";
			this.lblFechaHora.Size = new System.Drawing.Size(235, 25);
			this.lblFechaHora.TabIndex = 1;
			this.lblFechaHora.Text = "dddd, d \'de\' MMMM \'del\' yyyy, hh:mm:ss tt";
			this.lblFechaHora.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// lblTituloVentana
			// 
			this.lblTituloVentana.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.lblTituloVentana.BackColor = System.Drawing.Color.Transparent;
			this.lblTituloVentana.ForeColor = System.Drawing.Color.White;
			this.lblTituloVentana.Location = new System.Drawing.Point(150, 3);
			this.lblTituloVentana.Name = "lblTituloVentana";
			this.lblTituloVentana.Size = new System.Drawing.Size(400, 22);
			this.lblTituloVentana.TabIndex = 0;
			this.lblTituloVentana.Text = "Titulo";
			this.lblTituloVentana.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// frmContenedor
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.Black;
			this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.ClientSize = new System.Drawing.Size(800, 386);
			this.Controls.Add(this.pnlSistema);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.IsMdiContainer = true;
			this.MaximizeBox = false;
			this.Name = "frmContenedor";
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.Text = "SimuladorLinux";
			this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
			this.Load += new System.EventHandler(this.ContenedorMDI_Load);
			this.pnlSistema.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		public System.Windows.Forms.Panel pnlSistema;
		private System.Windows.Forms.Label lblTituloVentana;
		private System.Windows.Forms.Label lblFechaHora;
		public System.Windows.Forms.Timer tmrActualizadorHora;
		private System.Windows.Forms.Button btnApagar;
	}
}

