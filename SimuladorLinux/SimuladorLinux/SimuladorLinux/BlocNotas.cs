﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic;

namespace SimuladorLinux
{
	public partial class frmBlocNotas : Form
	{
		private bool cambiosPendientes;
		public frmBlocNotas()
		{
			InitializeComponent();
			frmContenedor.ConfigurarMdiParent(this);
			cambiosPendientes = false;
		}

		private void frmBlocNotas_Load(object sender, EventArgs e)
		{

		}

		private void NuevoToolStripMenuItem_Click(object sender, EventArgs e)
		{
			VerificarCambiosSinGuardar(sender, e, () =>
			{
				txtContenido.Clear();
				cambiosPendientes = false;
			});
		}

		private void AbrirToolStripMenuItem_Click(object sender, EventArgs e)
		{
			VerificarCambiosSinGuardar(sender, e, () => AbrirArchivo());
		}
		private void AbrirArchivo()
		{
			try
			{
				OpenFileDialog fileDialog = new OpenFileDialog();
				fileDialog.InitialDirectory = Application.StartupPath + "/Disco/";
				fileDialog.Filter = "Archivos de texto (*.txt)|*.txt|Todos los archivos (*.*)|*.*";
				fileDialog.CheckFileExists = true;
				fileDialog.Multiselect = false;
				fileDialog.Title = "Abrir archivo";
				DialogResult opcion = fileDialog.ShowDialog(this);
				if (opcion == DialogResult.OK)
				{
					txtContenido.Text = File.ReadAllText(fileDialog.FileName);
					cambiosPendientes = false;
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show("No hemos podido abrir el archivo por este error: \n" + ex.Message, "Error abriendo archivo", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		private void GuardarToolStripMenuItem_Click(object sender, EventArgs e)
		{
			try
			{
				SaveFileDialog saveDialog = new SaveFileDialog();
				saveDialog.InitialDirectory = Application.StartupPath + "/Disco/";
				saveDialog.Filter = "Archivo de texto (*.txt)|*.txt|HTML(*.html)|*.html|PHP(*.php)|*.php|Todos los archivos (*.*)|*.*";
				saveDialog.CheckPathExists = true;
				saveDialog.Title = "Guardar como";
				DialogResult opcion = saveDialog.ShowDialog(this);
				if (opcion == DialogResult.OK)
				{
					File.WriteAllText(saveDialog.FileName, txtContenido.Text);
					cambiosPendientes = false;
					if (e is FormClosingEventArgs)
					{
						((FormClosingEventArgs)e).Cancel = false;
					}
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show("No hemos podido guardar el archivo por este error: \n" + ex.Message, "Error guardando archivo", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		private void SalirToolStripMenuItem_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void CopiarToolStripMenuItem_Click(object sender, EventArgs e)
		{
			txtContenido.Copy();
		}

		private void PegarToolStripMenuItem_Click(object sender, EventArgs e)
		{
			txtContenido.Paste();
			cambiosPendientes = true;
		}

		private void CortarToolStripMenuItem_Click(object sender, EventArgs e)
		{
			txtContenido.Cut();
			cambiosPendientes = true;
		}

		private void DeshacerToolStripMenuItem_Click(object sender, EventArgs e)
		{
			txtContenido.Undo();
			cambiosPendientes = true;
		}

		private void RehacerToolStripMenuItem_Click(object sender, EventArgs e)
		{
			txtContenido.Redo();
			cambiosPendientes = true;
		}

		private void SelecionarToolStripMenuItem_Click(object sender, EventArgs e)
		{
			txtContenido.SelectAll();
		}

		private void DeselecionarToolStripMenuItem_Click(object sender, EventArgs e)
		{
			txtContenido.DeselectAll();
		}

		private void FuenteToolStripMenuItem_Click(object sender, EventArgs e)
		{
			try
			{
				FontDialog dlg = new FontDialog();
				dlg.Font = txtContenido.Font;
				if (dlg.ShowDialog() == DialogResult.OK)
				{
					txtContenido.Font = dlg.Font;
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show("No hemos podido cambiar la fuente del editor: \n" + ex.Message, "Error modificando fuente", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		private void AjusteDeLineaToolStripMenuItem_Click(object sender, EventArgs e)
		{
			ajusteDeLineaToolStripMenuItem.Checked = !ajusteDeLineaToolStripMenuItem.Checked;
			txtContenido.WordWrap = ajusteDeLineaToolStripMenuItem.Checked;
		}

		private void txtContenido_TextChanged(object sender, EventArgs e)
		{
			cambiosPendientes = true;
		}

		private void frmBlocNotas_FormClosing(object sender, FormClosingEventArgs e)
		{
			VerificarCambiosSinGuardar(sender, e, null);
		}
		private void VerificarCambiosSinGuardar(object sender, EventArgs e, Action action)
		{
			if (cambiosPendientes)
			{
				DialogResult opcion = MessageBox.Show(
					"¿Desea guardar los cambios pendientes antes de salir?",
					"Hay cambios pendientes sin guardar.",
					MessageBoxButtons.YesNoCancel,
					MessageBoxIcon.Question
				);
				if (opcion == DialogResult.Yes || opcion == DialogResult.Cancel)
				{
					if (e is FormClosingEventArgs)
					{
						((FormClosingEventArgs)e).Cancel = true;
					}
					if (opcion == DialogResult.Yes)
					{
						GuardarToolStripMenuItem_Click(sender, e);
					}
				}
				if (opcion == DialogResult.Cancel)
				{
					return;
				}
			}
			if (action != null) action.Invoke();
		}

		private void tsbAyuda_Click(object sender, EventArgs e)
		{
			frmAyuda.AbrirAyuda("blocnotas");
		}
	}
}
