﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SimuladorLinux
{
	public partial class frmExplorador : Form
	{
		static frmExplorador explorador;
		string rutaBase;
		string rutaInicio;
		public frmExplorador()
		{
			InitializeComponent();
			explorador = this;
			frmContenedor.ConfigurarMdiParent(this);
			rutaBase = Application.StartupPath + "/Disco";
			rutaInicio = RutaInicio();
		}

		private void Explorador_Load(object sender, EventArgs e)
		{
			wbrFolder.Url = new Uri(rutaInicio);
		}

		string RutaCompleta(string rutaParcial)
		{
			return rutaBase +
				(rutaParcial.StartsWith("/") ? "" : "/") +
				rutaParcial;
		}
		string RutaParcial(string rutaCompleta)
		{
			string[] parts = rutaCompleta.Split(new string[] { "/Disco" }, StringSplitOptions.RemoveEmptyEntries);
			if (parts.Length > 1)
				return parts[parts.Length - 1];
			return "/";
		}

		static public string RutaBase()
		{
			return Application.StartupPath + "/Disco/";
		}
		static public string RutaUsuario()
		{
			return RutaBase() + "Usuarios/" + Usuario.actual.usuario + "/";
		}
		static public string RutaInicio()
		{
			return RutaUsuario() + "Escritorio/";
		}

		static public void VerificarCrearCarpeta(string rutaCompleta)
		{
			DirectoryInfo carpeta = new DirectoryInfo(rutaCompleta);
			if (!carpeta.Exists)
			{
				carpeta.Create();
			}
		}
		static public void VerificarCrearCarpetaUsuario(string usuario)
		{
			VerificarCrearCarpeta(RutaBase() + "Usuarios/" + usuario + "/");
		}

		private void btnAtras_Click(object sender, EventArgs e)
		{
			if (wbrFolder.GoBack())
				wbrFolder.GoBack();
		}

		private void btnAlante_Click(object sender, EventArgs e)
		{
			if (wbrFolder.GoForward())
				wbrFolder.GoForward();

		}

		private void btnInicio_Click(object sender, EventArgs e)
		{
			wbrFolder.Navigate(rutaInicio);
		}

		private void btnNavegar_Click(object sender, EventArgs e)
		{
			wbrFolder.Navigate(RutaCompleta(txtRuta.Text));
		}

		private void btnRefrescar_Click(object sender, EventArgs e)
		{
			wbrFolder.Refresh();
		}

		private void wbrFolder_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
		{
			txtRuta.Text = RutaParcial(wbrFolder.Url.AbsolutePath);
			cbRutas.Items.Clear();
			string[] partes = txtRuta.Text.Split(new char[] { '/', '\\' }, StringSplitOptions.RemoveEmptyEntries);
			cbRutas.Items.AddRange(partes);
		}

		private void cbRutas_SelectedIndexChanged(object sender, EventArgs e)
		{
			List<string> partesRutaParcial = new List<string>();
			for (int i = 0; i <= cbRutas.SelectedIndex; i++)
			{
				partesRutaParcial.Add((string)cbRutas.Items[i]);
			}
			string rutaParcial = String.Join("/", partesRutaParcial);
			wbrFolder.Navigate(RutaCompleta(rutaParcial));
		}

		private void btnAyuda_Click(object sender, EventArgs e)
		{
			frmAyuda.AbrirAyuda("explorador");
		}
	}
}
