USE [master]
GO
/****** Object:  Database [SimuladorLinuxOneidy]    Script Date: 24-abr-20 12:35:46 AM ******/
CREATE DATABASE [SimuladorLinuxOneidy] ON  PRIMARY 
( NAME = N'SimuladorLinuxOneidy', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQL2017\MSSQL\DATA\SimuladorLinuxOneidy.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'SimuladorLinuxOneidy_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQL2017\MSSQL\DATA\SimuladorLinuxOneidy_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [SimuladorLinuxOneidy].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [SimuladorLinuxOneidy] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [SimuladorLinuxOneidy] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [SimuladorLinuxOneidy] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [SimuladorLinuxOneidy] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [SimuladorLinuxOneidy] SET ARITHABORT OFF 
GO
ALTER DATABASE [SimuladorLinuxOneidy] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [SimuladorLinuxOneidy] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [SimuladorLinuxOneidy] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [SimuladorLinuxOneidy] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [SimuladorLinuxOneidy] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [SimuladorLinuxOneidy] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [SimuladorLinuxOneidy] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [SimuladorLinuxOneidy] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [SimuladorLinuxOneidy] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [SimuladorLinuxOneidy] SET  DISABLE_BROKER 
GO
ALTER DATABASE [SimuladorLinuxOneidy] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [SimuladorLinuxOneidy] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [SimuladorLinuxOneidy] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [SimuladorLinuxOneidy] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [SimuladorLinuxOneidy] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [SimuladorLinuxOneidy] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [SimuladorLinuxOneidy] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [SimuladorLinuxOneidy] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [SimuladorLinuxOneidy] SET  MULTI_USER 
GO
ALTER DATABASE [SimuladorLinuxOneidy] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [SimuladorLinuxOneidy] SET DB_CHAINING OFF 
GO
USE [SimuladorLinuxOneidy]
GO
/****** Object:  Table [dbo].[HistorialAccesos]    Script Date: 24-abr-20 12:35:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HistorialAccesos](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Fecha] [datetime] NOT NULL,
	[Usuario] [varchar](100) NOT NULL,
	[Nombre] [varchar](100) NOT NULL,
 CONSTRAINT [PK_HistorialAccesos] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Usuarios]    Script Date: 24-abr-20 12:35:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Usuarios](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](100) NOT NULL,
	[Usuario] [varchar](100) NOT NULL,
	[Contrasenia] [varchar](100) NOT NULL,
	[Tipo] [varchar](20) NOT NULL,
 CONSTRAINT [PK_Usuarios] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[HistorialAccesos] ON 

INSERT [dbo].[HistorialAccesos] ([Id], [Fecha], [Usuario], [Nombre]) VALUES (1, CAST(N'2020-04-23T22:47:27.483' AS DateTime), N'one', N'Oneidy')
INSERT [dbo].[HistorialAccesos] ([Id], [Fecha], [Usuario], [Nombre]) VALUES (2, CAST(N'2020-04-23T22:49:36.760' AS DateTime), N'one', N'Oneidy')
INSERT [dbo].[HistorialAccesos] ([Id], [Fecha], [Usuario], [Nombre]) VALUES (3, CAST(N'2020-04-23T22:52:55.153' AS DateTime), N'one', N'Oneidy')
INSERT [dbo].[HistorialAccesos] ([Id], [Fecha], [Usuario], [Nombre]) VALUES (4, CAST(N'2020-04-23T22:54:40.087' AS DateTime), N'one', N'Oneidy')
INSERT [dbo].[HistorialAccesos] ([Id], [Fecha], [Usuario], [Nombre]) VALUES (5, CAST(N'2020-04-23T23:00:45.547' AS DateTime), N'one', N'Oneidy')
INSERT [dbo].[HistorialAccesos] ([Id], [Fecha], [Usuario], [Nombre]) VALUES (6, CAST(N'2020-04-23T23:09:33.247' AS DateTime), N'one', N'Oneidy')
INSERT [dbo].[HistorialAccesos] ([Id], [Fecha], [Usuario], [Nombre]) VALUES (7, CAST(N'2020-04-23T23:37:05.817' AS DateTime), N'one', N'Oneidy')
INSERT [dbo].[HistorialAccesos] ([Id], [Fecha], [Usuario], [Nombre]) VALUES (8, CAST(N'2020-04-23T23:55:15.463' AS DateTime), N'one', N'Oneidy')
INSERT [dbo].[HistorialAccesos] ([Id], [Fecha], [Usuario], [Nombre]) VALUES (9, CAST(N'2020-04-24T00:12:50.333' AS DateTime), N'one', N'Oneidy')
INSERT [dbo].[HistorialAccesos] ([Id], [Fecha], [Usuario], [Nombre]) VALUES (10, CAST(N'2020-04-24T00:16:59.967' AS DateTime), N'one', N'Oneidy')
INSERT [dbo].[HistorialAccesos] ([Id], [Fecha], [Usuario], [Nombre]) VALUES (11, CAST(N'2020-04-24T00:17:56.433' AS DateTime), N'one', N'Oneidy')
INSERT [dbo].[HistorialAccesos] ([Id], [Fecha], [Usuario], [Nombre]) VALUES (12, CAST(N'2020-04-24T00:28:20.323' AS DateTime), N'one', N'Oneidy')
INSERT [dbo].[HistorialAccesos] ([Id], [Fecha], [Usuario], [Nombre]) VALUES (13, CAST(N'2020-04-24T00:30:39.657' AS DateTime), N'one', N'Oneidy')
SET IDENTITY_INSERT [dbo].[HistorialAccesos] OFF
SET IDENTITY_INSERT [dbo].[Usuarios] ON 

INSERT [dbo].[Usuarios] ([Id], [Nombre], [Usuario], [Contrasenia], [Tipo]) VALUES (1, N'Administrador', N'admin', N'1234', N'Administrador')
INSERT [dbo].[Usuarios] ([Id], [Nombre], [Usuario], [Contrasenia], [Tipo]) VALUES (3, N'Invitado', N'guest', N'', N'Invitado')
INSERT [dbo].[Usuarios] ([Id], [Nombre], [Usuario], [Contrasenia], [Tipo]) VALUES (4, N'Oneidy', N'one', N'1234', N'Administrador')
INSERT [dbo].[Usuarios] ([Id], [Nombre], [Usuario], [Contrasenia], [Tipo]) VALUES (5, N'Reny Serrano', N'reny', N'1234', N'Estándar')
INSERT [dbo].[Usuarios] ([Id], [Nombre], [Usuario], [Contrasenia], [Tipo]) VALUES (6, N'Abel Matos', N'amatoszds', N'1234', N'Administrador')
INSERT [dbo].[Usuarios] ([Id], [Nombre], [Usuario], [Contrasenia], [Tipo]) VALUES (12, N'eqwe', N'hjj', N'kjh', N'Administrador')
INSERT [dbo].[Usuarios] ([Id], [Nombre], [Usuario], [Contrasenia], [Tipo]) VALUES (13, N'uiuwreh', N'kjhgfndifo', N'kjno', N'Administrador')
SET IDENTITY_INSERT [dbo].[Usuarios] OFF
USE [master]
GO
ALTER DATABASE [SimuladorLinuxOneidy] SET  READ_WRITE 
GO
